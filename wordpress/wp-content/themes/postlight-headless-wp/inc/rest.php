<?php

function get_theme_options() {
    $facebookUrl = toz_option('Facebook url' , '99558724036');
    $instagramUrl = toz_option('Instagram url' , '47558740173');
    $youtubeUrl = toz_option('Youtube url' , '67558757845');
    $twitterUrl = toz_option('Twitter url' , '10558804317');
    $footerContact = toz_option('Footer contact' , '37558844685');
    $footerTeatrVariete = toz_option('Footer teatr variete' , '95558868116');
    $footerInfo = toz_option('Footer info' , '32199673964');
    $privacyPolicyLabel = toz_option('Privacy policy label' , '1199700012');
    $privacyPolicyLink = toz_option('Privacy policy link' , '74199700828');
    $fbAppId = toz_option('Facebook APP ID' , '22819558609');
    $accessibleLink = toz_option('Accessible link' , '50141405110');
    $data = new StdClass();
    $data->socials = new StdClass();
    $data->socials->facebookUrl = $facebookUrl;
    $data->socials->instagramUrl = $instagramUrl;
    $data->socials->youtubeUrl = $youtubeUrl;
    $data->socials->twitterUrl = $twitterUrl;
    $data->footerContact = $footerContact;
    $data->footerTeatrVariete = $footerTeatrVariete;
    $data->footerInfo = $footerInfo;
    $data->privacyPolicy = new StdClass();
    $data->privacyPolicy->label = $privacyPolicyLabel;
    $data->privacyPolicy->url = $privacyPolicyLink;
    $data->fbAppId =$fbAppId;
    $data->accessibleLink =$accessibleLink;
    return $data;
}

function get_posts_years_array() {
    global $wpdb;
    $result = array();
    $years = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT YEAR(post_date) FROM {$wpdb->posts} WHERE post_status = 'publish' AND post_type = 'post' GROUP BY YEAR(post_date) DESC"
        ),
        ARRAY_N
    );
    if ( is_array( $years ) && count( $years ) > 0 ) {
        foreach ( $years as $year ) {
            $result[] = $year[0];
        }
    }
    return $result;
}

function get_posts_categories_by_type($postType = 'post') {
    if( !function_exists('tartist__get_terms_by_post_type') ){

        function tartist__get_terms_by_post_type($taxonomy = 'category'){

            /**
             * Get Terms by post type
             * @author Amit Biswas (https://templateartist.com/)
             * @link (https://templateartist.com/2020/05/18/get-categories-by-post-type-in-wordpress/)
             *
             * @param postType default 'post'
             * @param taxonomy default 'category'
             *
             * @return array of terms for a given $posttype and $taxonomy
             * @since 1.0.1
             *
             * 1. Get all posts by post type
             * 2. Loop through the posts array and retrieve the terms attached to those posts
             * 3. Store the new terms objects within `$post_terms`
             * 4. loop through `$post_terms` as it's a array of term objects.
             * 5. store the terms with our desired key, value pair inside `$post_terms_array`
             */

            //1. Get all posts by post type
            $get_all_posts = get_posts( array(
                'post_type'     => esc_attr( $postType ),
                'post_status'   => 'publish',
                'numberposts'   => -1
            ) );

            if( !empty( $get_all_posts ) ){

                //First Empty Array to store the terms
                $post_terms = array();

                //2. Loop through the posts array and retrieve the terms attached to those posts
                foreach( $get_all_posts as $all_posts ){

                    /**
                     * 3. Store the new terms objects within `$post_terms`
                     */
                    $post_terms[] = get_the_terms( $all_posts->ID, esc_attr( $taxonomy ) );

                }

                //Second Empty Array to store final term data in key, value pair
                $post_terms_array = array();

                /**
                 * 4. loop through `$post_terms` as it's a array of term objects.
                 */

                foreach($post_terms as $new_arr){
                    foreach($new_arr as $arr){

                        /**
                         * 5. store the terms with our desired key, value pair inside `$post_terms_array`
                         */
                        $post_terms_array[] = array(
                            'name'      => $arr->name,
                            'term_id'   => $arr->term_id,
                            'slug'      => $arr->slug,
                            'url'       => get_term_link( $arr->term_id )
                        );
                    }
                }

                //6. Make that array unique as duplicate entries can be there
                $terms = array_unique($post_terms_array, SORT_REGULAR);

                //7. Return the final array
                return array_values($terms);

            }

        }

        return tartist__get_terms_by_post_type();
    }
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'custom', '/theme-options', array(
    'methods' => 'GET',
    'callback' => 'get_theme_options',
  ) );

  register_rest_route( 'custom', '/posts-years', array(
      'methods' => 'GET',
      'callback' => 'get_posts_years_array',
    ) );

  register_rest_route( 'custom', '/posts-categories', array(
      'methods' => 'GET',
      'callback' => 'get_posts_categories_by_type',
    ) );
} );

add_filter( 'rest_prepare_post', function( $response, $post, $request ) {

  // Only do this for single post requests.
  if( $request->get_param('per_page') === 1 ) {
        global $post;
        // Get the so-called next post.
        $next = get_adjacent_post( false, '', false );
        // Get the so-called previous post.
        $previous = get_adjacent_post( false, '', true );
        // Format them a bit and only send id and slug (or null, if there is no next/previous post).
        $response->data['next'] = ( is_a( $next, 'WP_Post') ) ? array( "id" => $next->ID, "slug" => $next->post_name, "title" => $next->post_title) : null;
        $response->data['previous'] = ( is_a( $previous, 'WP_Post') ) ? array( "id" => $previous->ID, "slug" => $previous->post_name, "title" => $previous->post_title) : null;
  }

    return $response;
}, 10, 3 );

