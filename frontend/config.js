const frontendUrl = 'http://frontend:3000';
const publicFrontendUrl = 'http://frontend.variette.localhost';

// If we're running on Docker, use the WordPress container hostname instead of localhost.
if (process.env.HOME === '/home/node') {
  // apiUrl = 'http://wp-headless:8080/wp-json';
}

const Config = {
  pl: {
    apiUrl: `http://wordpress.variette.localhost/wp-json`,
    wpUrl: 'http://wordpress.variette.localhost',
    locale: 'pl_PL',
    title: 'Krakowski Teatr VARIETE',
    publicFrontendUrl,
    frontendUrl,
    AUTH_TOKEN: 'auth-token',
    freshmailToken: 'Bearer mWK6l7.lAwVwkmrEkIY0GqaOYyytGwy4qMlrPcE6K2',
    freshmailListHash: '1m7qvfipvg',
    smtp: {
      host: 'teatrvariete.nazwa.pl',
      port: 465,
      smtpPass: 'dzxKEu<Z/@%Q<ZR2',
      smtpEmail: 'formularz@teatrvariete.pl',
      smtpName: 'Teatr Variete',
      adminEmail: 'sekretariat@teatrvariete.pl',
    }
  },
  en: {
    apiUrl: `http://en.wordpress.variette.localhost/wp-json`,
    wpUrl: 'http://en.wordpress.variette.localhost',
    locale: 'en_US',
    title: 'Krakowski Teatr VARIETE',
    publicFrontendUrl,
    frontendUrl,
    AUTH_TOKEN: 'auth-token',
    freshmailToken: 'Bearer mWK6l7.lAwVwkmrEkIY0GqaOYyytGwy4qMlrPcE6K2',
    freshmailListHash: '1m7qvfipvg',
    smtp: {
      host: 'teatrvariete.nazwa.pl',
      port: 465,
      smtpPass: 'dzxKEu<Z/@%Q<ZR2',
      smtpEmail: 'formularz@teatrvariete.pl',
      smtpName: 'Teatr Variete',
      adminEmail: 'sekretariat@teatrvariete.pl',
    }
  }
};

export default Config;
