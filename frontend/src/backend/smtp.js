import nodemailer from 'nodemailer';
import ejs from 'ejs';

export const createTransporter = (smtp) =>
  nodemailer.createTransport({
    host: smtp.host,
    port: smtp.port,
    auth: {
      user: smtp.smtpEmail,
      pass: smtp.smtpPass,
    },
  });

export const sendEmail = ({
  sendFromEmail,
  sendFromName,
  sendToEmail,
  subject,
  formData,
  templatePath,
  transporter,
}) => {
  const from =
    sendFromName && sendFromEmail
      ? `${sendFromName} <${sendFromEmail}>`
      : `${sendFromName || sendFromEmail}`;
  ejs.renderFile(templatePath, formData, function (err, html) {
    if (err) {
      console.log(err);
      return false;
    }

    const message = {
      from,
      to: `${sendToEmail}`,
      subject,
      html,
      replyTo: from,
    };

    return new Promise((resolve, reject) => {
      transporter.sendMail(message, (error, info) =>
        error ? reject(error) : resolve(info),
      );
    });
  });
};
