export default {
  pl: {
    required: 'To pole jest wymagane',
    phone: 'Podaj poprawny numer telefonu',
    email: 'Podaj poprawny adres e-mail',
  },
  en: {
    required: 'This field is required',
    phone: 'This is not valid phone number',
    email: 'This is not valid e-mail address',
  }
}
