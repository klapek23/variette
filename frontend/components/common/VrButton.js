import React from 'react';
import classNames from 'classnames';

const VrButton = (
  {
    children,
    color,
    type = 'button',
    small = false,
    big = false,
    icon = false,
    noPadding = false,
    noBorder = false,
    smallWithBigText = false,
    disabled = false,
    className = '',
    onClick = () => null
  }
) => {
  const classnames = classNames('vr-button', className, {
    'vr-button--small': small,
    'vr-button--big': big,
    'vr-button--icon': icon,
    'vr-button--no-padding': noPadding,
    'vr-button--no-border': noBorder,
    'vr-button--small-with-big-text': smallWithBigText,
  });
  return (
    <button
      type={type}
      disabled={disabled}
      className={`${classnames} ${color ? `vr-button--${color}` : ''}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
}

export default VrButton;
