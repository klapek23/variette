import React, { useState } from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import OutsideClickHandler from 'react-outside-click-handler';
import shortid from 'shortid';

export default function VrRepertoireSelect({
  months,
  activeMonth,
  handleMonthClick,
}) {
  const [active, setActive] = useState(false);

  const toggleSelect = () => setActive(!active);

  const handleClickedOutside = () => setActive(false);

  const handleItemClick = ({ name, value, ...rest }) => {
    handleMonthClick({ name, value, ...rest });
    setActive(false);
  };

  const currentMonth = months.find(({ value }) => value === activeMonth.toString());

  return (
    <OutsideClickHandler onOutsideClick={handleClickedOutside}>
      <div className="vr-repertoire-select">
        <button className="vr-repertoire-select__current" onClick={toggleSelect}>
          <h2>{currentMonth ? currentMonth.name : ''}</h2>
          <ExpandMoreIcon className="vr-repertoire-select__current-icon" />
        </button>

        <AnimatePresence>
          {active && (
            <motion.div
              className="vr-repertoire-select__list-container"
              initial={{ opacity: 0, height: '0px' }}
              animate={{ opacity: 1, height: 'auto' }}
              exit={{ opacity: 0, height: '0px' }}
            >
              <ul className="vr-repertoire-select__list">
                {months.map(({ name, value, ...rest }) => (
                  <li
                    key={shortid.generate()}
                    className={`vr-repertoire-select__list-item ${
                      value === activeMonth
                        ? 'vr-repertoire-select__list-item--active'
                        : ''
                    }`}
                  >
                    <button onClick={() => handleItemClick({ name, value, ...rest })}>
                      {name}
                    </button>
                  </li>
                ))}
              </ul>
            </motion.div>
          )}
        </AnimatePresence>
      </div>
    </OutsideClickHandler>
  );
}
