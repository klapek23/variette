import React from 'react';
import VrLink from './VrLink';

export default function VrContentBoxesItem({
   link,
   image,
   title,
   excerpt,
   bgText,
   main,
   buttonLabel,
}) {
  return (
    <VrLink
      className="vr-content-boxes__item"
      as={link.as}
      href={link.href}
      title={link.title}
    >
      <div className="vr-content-boxes__item-image">
          {image && <img src={image.url} alt={image.title && image.title.rendered ? image.title.rendered : ''} />}
      </div>
      <article
        className={`vr-content-boxes__item-caption ${
          bgText ? 'vr-content-boxes__item-caption--with-bgtext' : ''
        }`}
      >
        {bgText ? (
          <span className="vr-content-boxes__item-bgtext">{bgText}</span>
        ) : null}
        {main
            ? <h2 dangerouslySetInnerHTML={{__html: title.rendered || title }}/>
            : <h3 dangerouslySetInnerHTML={{__html: title.rendered || title }}/>
        }
        {excerpt && excerpt.length > 10 ? (
          <div dangerouslySetInnerHTML={{__html: excerpt}}/>
        ) : null}
      </article>
      <span className="vr-button vr-button--small-with-big-text">{buttonLabel}</span>
    </VrLink>
  );
}
