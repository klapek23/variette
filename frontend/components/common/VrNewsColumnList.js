import React from 'react';
import shortid from 'shortid';
import classNames from 'classnames';

import VrLink from './VrLink';

export default function VrNewsColumnList({
  title,
  items,
  paddingBottom = false,
}) {
  const classnames = classNames('vr-news-column-list', 'vr-section', {
    'vr-section--no-bottom-padding': !paddingBottom,
  });
  return (
    <div className={classnames}>
      <h3>{title}</h3>
      <div className="vr-news-column-list__list">
        {items.map(({title: newsTitle, url}) => (
          <VrLink
            as={url}
            href={url}
            key={shortid.generate()}
            title={newsTitle}
            className="vr-news-column-list__list-item"
          >
            {newsTitle}
          </VrLink>
        ))}
      </div>
    </div>
  );
}
