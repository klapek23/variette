import React, { Component } from 'react';
import { Swipeable } from 'react-swipeable';
import debounce from '../../utils/debounce';
import VrArrowButton from './VrArrowButton';

const ArrowLeft = ({ onClick }) => (
  <VrArrowButton
    direction="left"
    onClick={onClick}
    className="vr-repertoire-months__arrow-button vr-repertoire-months__arrow-button--prev"
  />
);

const ArrowRight = ({ onClick }) => (
  <VrArrowButton
    direction="right"
    onClick={onClick}
    className="vr-repertoire-months__arrow-button vr-repertoire-months__arrow-button--next"
  />
);

export default class VrRepertoireMonthsSlider extends Component {
  viewport = React.createRef();

  constructor(props) {
    super(props);
    this.state = {
      months: props.months,
      listWidth: 0,
      listPosition: 0,
      oneSlideChangeWidth: 200,
    };
    const createRefs = () => {
      for (let i = 0; i <= props.months.length; i += 1) {
        this[`month-ref-${i}`] = React.createRef();
      }
    };

    createRefs();
  }

  componentDidMount() {
    this.countMonthsWidth();
    const windowWidth = document.documentElement.clientWidth;
    this.setOnslideChangeWidth(windowWidth);

    window.addEventListener('resize', this.handleWindowResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize);
  }

  handlePrevClick = () => {
    const { listPosition, oneSlideChangeWidth } = this.state;
    const nextPosition = listPosition + oneSlideChangeWidth;
    const calculatePosition = () => {
      return nextPosition <= 0 ? nextPosition : 0;
    };
    this.setState({
      listPosition: calculatePosition(),
    });
  };

  handleNextClick = () => {
    const { listPosition, listWidth, oneSlideChangeWidth } = this.state;
    const viewportWidth = this.viewport.current.offsetWidth;
    const nextPosition = listPosition - oneSlideChangeWidth;
    const calculatePosition = () => {
      return nextPosition > -(listWidth - viewportWidth)
        ? nextPosition
        : -(listWidth - viewportWidth);
    };
    this.setState({
      listPosition: calculatePosition(),
    });
  };

  handleMonthClick = ({ name, value, ...rest }) => {
    const { handleMonthClick } = this.props;
    const { months, listWidth, listPosition } = this.state;
    const viewportWidth = this.viewport.current.offsetWidth;
    const clickedMonth = months.find((m) => m.value === value);
    const nextPosition =
      -(clickedMonth.offsetLeft + clickedMonth.width / 2) + viewportWidth / 2;
    if (nextPosition > listPosition) {
      this.setState({
        listPosition: nextPosition <= 0 ? nextPosition : 0,
      });
    } else {
      this.setState({
        listPosition:
          nextPosition > -(listWidth - viewportWidth)
            ? nextPosition
            : -(listWidth - viewportWidth),
      });
    }
    handleMonthClick({ name, value, ...rest });
  };

  handleWindowResize = () => {
    debounce(this.countMonthsWidth());
    const windowWidth = document.documentElement.clientWidth;
    this.setOnslideChangeWidth(windowWidth);
  };

  setOnslideChangeWidth(windowWidth) {
    this.setState({
      oneSlideChangeWidth: windowWidth >= 767 ? 200 : 100,
    });
  }

  countMonthsWidth = () => {
    const { months } = this.state;
    this.setState(
      {
        months: months.map((item, index) => ({
          ...item,
          width: this[`month-ref-${index}`].current.offsetWidth,
          offsetLeft: this[`month-ref-${index}`].current.offsetLeft,
        })),
      },
      () => this.countListWidth(),
    );
  };

  countListWidth = () => {
    const { months } = this.state;
    this.setState({
      listWidth: months.reduce((acc, current) => {
        return acc + current.width;
      }, 0),
    });
  };

  render() {
    const { months, listPosition, listWidth } = this.state;
    const { activeMonth } = this.props;
    return (
      <div className="vr-repertoire-months">
        <div className="vr-repertoire-months__container">
          <ArrowLeft onClick={this.handlePrevClick} />
          <div className="vr-repertoire-months__viewport" ref={this.viewport}>
            <Swipeable
              onSwipedLeft={this.handleNextClick}
              onSwipedRight={this.handlePrevClick}
            >
              <div
                className="vr-repertoire-months__list"
                style={{
                  transform: `translateX(${listPosition}px)`,
                  width: `${listWidth}px`,
                }}
              >
                {months.map(({ name, value, ...rest }, i) => (
                  <button
                    type="button"
                    onClick={() =>
                      this.handleMonthClick({
                        name,
                        value,
                        ...rest,
                      })
                    }
                    className={`vr-repertoire-months__list-item ${
                      activeMonth === value
                        ? 'vr-repertoire-months__list-item--active'
                        : ''
                    }`}
                    key={`month-${value}`}
                    ref={this[`month-ref-${i}`]}
                  >
                    <h2>{name}</h2>
                  </button>
                ))}
              </div>
            </Swipeable>
          </div>
          <ArrowRight onClick={this.handleNextClick} />
        </div>
      </div>
    );
  }
}
