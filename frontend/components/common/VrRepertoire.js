import React, { Component } from 'react';
import moment from 'moment';
import shortid from 'shortid';

import VrRepertoireMonthsSlider from './VrRepertoireMonthsSlider';
import VrLink from './VrLink';
import VrSectionBanner from './VrSectionBanner';
import VrNoResults from './VrNoResults';
import VrContentSpinner from './VrContentSpinner';
import VrRepertoireSelect from './VrRepertoireSelect';

const RepertoireItem = ({ title, direction, date, category, slug, labels, buy }) => {
  const monthDay = moment(date).format('DD/M');
  const day = moment(date).format('dddd');
  const time = moment(date).format('HH:mm');

  return (
    <div className="vr-repertoire__item">
      <VrLink
        as={`/repertuar/${category.slug}/${slug}`}
        href="/repertuar/[type]/[slug]"
      >
        <div className="vr-repertoire__item-info">
          <div className="vr-repertoire__item-info-container">
            <div className="vr-repertoire__item-info-box vr-repertoire__item-info-box--datetime">
              <div className="vr-repertoire__item-info-date">{monthDay}</div>
              <div className="vr-repertoire__item-info-time">
                {day} <br /> {time}
              </div>
            </div>
            <div className="vr-repertoire__item-info-box">
              <div className="vr-repertoire__item-info-caption">
                <h3 className="vr-repertoire__item-info-caption-title" dangerouslySetInnerHTML={{ __html: title.rendered }} />
                <p className="vr-repertoire__item-info-caption-subtitle">
                  {direction}
                </p>
              </div>
            </div>
          </div>
          <span className="vr-repertoire__item-info-more">{labels.more}</span>
        </div>
      </VrLink>
      <div className="vr-repertoire__item-actions">
        <VrLink as={buy.url} target={buy.target} className="vr-repertoire__item-actions--buy">
          {buy.title}
        </VrLink>
      </div>
    </div>
  );
};

const NoResults = ({ label }) => (
  <div className="vr-repertoire__no-results">
    <VrNoResults label={label} />
  </div>
);

export default class VrRepertoire extends Component {

  constructor(props) {
    super(props);
    const months = this.createMonthsList();
    this.state = {
      months,
    }
  }

  createMonthsList = () => {
    const months = [];
    const dateStart = moment();
    const dateEnd = moment().add(7, 'month');

    while (dateEnd.diff(dateStart, 'months') >= 0) {
      const name = dateStart.format('MMMM');
      const startDate = dateStart.startOf('month').format('YYYY/MM/D');
      const endDate = dateStart.endOf('month').format('YYYY/MM/D');
      const value = dateStart.format('M');
      months.push({ name, value, startDate, endDate });

      dateStart.add(1, 'month');
    }
    return months;
  };

  handleMonthClick = ({ value, startDate, endDate }) => {
    const { onMonthChange } = this.props;
    onMonthChange(value, startDate, endDate);
  };

  setButtonsWidth = () => {
    const buttons = document.getElementsByClassName('vr-repertoire__month');
    for (let button of buttons) {
      const width = button.children.item(0).offsetWidth;
      button.style.width = `${width}px`;
    }
  };

  renderItems = (items) => {
    const { labels } = this.props;
    return items && items.length > 0 ? (
      items.map(({ date, direction, buy, title: itemTitle, slug, category }) => (
        <RepertoireItem
          key={shortid.generate()}
          title={itemTitle}
          date={date}
          direction={direction}
          category={category}
          buy={buy}
          slug={slug}
          labels={labels}
        />
      ))
    ) : (
      <NoResults label={labels.noResults} />
    );
  };

  render() {
    const { title, subtitle, activeMonth, items, loading } = this.props;
    const { months } = this.state;

    return (
      <div className="vr-section vr-repertoire">
        <VrSectionBanner title={title} subtitle={subtitle} main>
          <VrRepertoireMonthsSlider
            activeMonth={activeMonth}
            months={months}
            handleMonthClick={this.handleMonthClick}
          />

          <VrRepertoireSelect
            activeMonth={activeMonth}
            months={months}
            handleMonthClick={this.handleMonthClick}
          />
        </VrSectionBanner>
        <div className="container-fluid">
          <div className="row center-xs">
            <div className="col-xs-12">
              <div className="vr-repertoire__list">
                {loading ? (
                  <VrContentSpinner isVisible={loading} />
                ) : (
                  this.renderItems(items)
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
