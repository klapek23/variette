import React from 'react';
import classNames from 'classnames';

import VrButton from './VrButton';
import VrLink from './VrLink';

const VrContent04 = ({
  title,
  link,
  leftColumn,
  rightColumn,
  equalColumns = false,
  bothText = false,
  alternate
}) => {
  const classnames = classNames('vr-content-04', 'vr-section', {
    'vr-content-04--equal-columns': equalColumns,
    'vr-content-04--both-text': bothText,
    'vr-content-04--alternate': alternate,
  });

  return (
    <div className={classnames}>
      <div className="container-fluid">
        {bothText && (
          <div className="row">
            <div className="col-xs-12">
              <h3>{title}</h3>
            </div>
          </div>
        )}
        <div className="row">
          <div
            className={
              !equalColumns ? `col-xs-12 col-lg-8` : 'col-xs-12 col-lg-6'
            }
          >
            <div className="vr-content-04__content">
              {!bothText && <h3>{title}</h3>}
              {leftColumn}
              {link && (
                <VrLink as={link.url} title={link.title} target={link.target}>
                  <VrButton color="black">{link.title}</VrButton>
                </VrLink>
              )}
            </div>
          </div>
          <div
            className={
              !equalColumns ? `col-xs-12 col-lg-4` : `col-xs-12 col-lg-6`
            }
          >
            <div
              className={`${
                bothText ? 'vr-content-04__content' : 'vr-content-04__image'
              }`}
            >
              {rightColumn}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VrContent04;
