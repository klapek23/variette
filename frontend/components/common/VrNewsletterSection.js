import React, { useEffect, useState } from 'react';
import useIsInViewport from 'use-is-in-viewport';
import Vivus from 'vivus';

import MusicSvg from '../../static/music.svg';
import VrSectionBanner from './VrSectionBanner';
import VrNewsletterForm from './forms/VrNewsletterForm';

export default function VrNewsletterSection({
  title,
  subtitle,
  fields,
  submitLabel,
  formUrl,
  successMessage,
}) {
  const [vivus, setVivus] = useState(null);
  const [isInViewport, targetRef] = useIsInViewport({ threshold: 20 })

  useEffect(() => {
    setVivus(
      new Vivus(
        'music-svg',
        {
          duration: 150,
          start: 'manual',
        },
        () => {
          const animatedElement = document.getElementById('music-svg');
          if (animatedElement) {
            animatedElement.classList.add('ready');
          }
        },
      ),
    );
  }, []);

  if (isInViewport) {
    vivus.play();
  }

  return (
    <div className="vr-newsletter-section vr-section">
      <div className="vr-newsletter-section__svg" ref={targetRef}>
        <MusicSvg id="music-svg" />
      </div>

      <VrSectionBanner title={title} subtitle={subtitle} inverted />
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12 col-lg-10 col-xl-9">
            <VrNewsletterForm
              fields={fields}
              submitLabel={submitLabel}
              successMessage={successMessage}
              url={formUrl}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
