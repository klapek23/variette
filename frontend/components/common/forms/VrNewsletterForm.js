import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import FormHelperText from '@material-ui/core/FormHelperText';
import axios from 'axios';
import VrButton from '../VrButton';
import { VrCheckboxLabel } from './VrCheckbox';

export default function VrNewsletterForm({
  fields,
  url = '/api/newsletter-form',
  submitLabel,
  successMessage,
  successCallback
}) {
  const getDefaultState = () =>
    fields
      .filter(({ type }) => type !== 'info')
      .reduce((prev, { name, defaultValue, type, fields: subfields }) => {
        if (type === 'double') {
          return {
            ...prev,
            ...subfields.reduce((prev, current) => {
              return { ...prev, [current.name]: current.defaultValue };
            }, {}),
          };
        }

        return {
          ...prev,
          [name]: defaultValue,
        };
      }, {});

  const [form, setForm] = useState(getDefaultState());
  const [formErrors, setFormErrors] = useState(null);
  const [success, setSuccess] = useState(false);

  const validateInput = (name, value) => {
    let field = fields.find(
      ({ name: fieldName, type: fieldType, fields: subfields }) => {
        if (fieldType === 'double') {
          const subfield = subfields.find(
            ({ name: subfieldName }) => subfieldName === name,
          );
          if (subfield) {
            return subfield;
          }
        }
        return name === fieldName;
      },
    );

    if (field.type === 'double') {
      field = field.fields.find(({ name: fieldName }) => fieldName === name);
    }
    return field.validationFn ? field.validationFn(value) : null;
  };

  const validateForm = () => {
    const formFields = Object.keys(form);
    let errors = {};
    formFields.forEach((fieldName) => {
      const value = form[fieldName];
      const error = validateInput(fieldName, value);
      if (error) {
        errors[fieldName] = error;
      }
    });

    errors = Object.keys(errors).length > 0 ? errors : null;
    setFormErrors(errors);

    return !errors;
  };

  const handleChange = ({ target }) => {
    const { name, value, type, checked } = target;
    let error;
    if (type === 'checkbox') {
      setForm({ ...form, [name]: checked });
      error = validateInput(name, checked);
    } else {
      setForm({ ...form, [name]: value });
      error = validateInput(name, value);
    }

    setFormErrors({ ...formErrors, [name]: error });
  };

  const sendData = async (formData, sendUrl) => {
    try {
      const res = await axios({
        method: 'post',
        url: sendUrl,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        data: formData,
      });
      return res;
    } catch (error) {
      return error;
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = { ...form };

    if (validateForm()) {
      const { data: result } = await sendData(data, url);
      if (result && result.success) {
        setForm(getDefaultState());
        setFormErrors(null);
        if (successCallback) {
          successCallback();
        } else {
          setSuccess(true);
          setTimeout(() => {
            setSuccess(false);
          }, 5000);
        }
      } else {
        setFormErrors(result ? result.errors : undefined);
      }
    }
  };

  const findField = (name) => {
    return fields.find((field) => field.name === name);
  };

  const { name, email, rodo } = form;

  return success ? (
    <h4>{successMessage}</h4>
  ) : (
    <form
      className="vr-newsletter-section__form"
      noValidate
      autoComplete="off"
      onChange={handleChange}
      onSubmit={handleSubmit}
    >
      <div className="vr-newsletter-section__form-container">
        <div className="vr-newsletter-section__form-field-box vr-newsletter-section__form-field-box--text">
          <TextField
            className="vr-newsletter-section__form-field"
            name="name"
            id="name"
            value={name}
            required
            label={findField('name').fieldProps.label}
            error={formErrors && !!formErrors.name}
            helperText={formErrors ? formErrors.name : ''}
          />
        </div>
        <div className="vr-newsletter-section__form-field-box vr-newsletter-section__form-field-box--text">
          <TextField
            className="vr-newsletter-section__form-field"
            id="email"
            name="email"
            label={findField('email').fieldProps.label}
            value={email}
            required
            error={formErrors && !!formErrors.email}
            helperText={formErrors ? formErrors.email : ''}
          />
        </div>
        <div className="vr-newsletter-section__form-field-box vr-newsletter-section__form-field-box--submit">
          <VrButton type="submit">{submitLabel}</VrButton>
        </div>
        <div className="vr-newsletter-section__form-field-box vr-newsletter-section__form-field-box--checkbox">
          <FormControl required error={formErrors && !!formErrors.rodo}>
            <VrCheckboxLabel
              label={findField('rodo').fieldProps.label}
              control={
                <Checkbox name="rodo" value={rodo} checked={rodo} required />
              }
            />
            <FormHelperText>
              {formErrors ? formErrors.rodo : null}
            </FormHelperText>
          </FormControl>
        </div>
      </div>
    </form>
  );
}
