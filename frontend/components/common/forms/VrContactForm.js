import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import useIsInViewport from 'use-is-in-viewport';
import Vivus from 'vivus';
import { useRouter } from 'next/router';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import axios from 'axios';
import { VrCheckboxLabel } from './VrCheckbox';
import VrSectionBanner from '../VrSectionBanner';
import VrButton from '../VrButton';
import ContactSvg from '../../../static/contact.svg';

export default function VrContactForm({
  fields,
  title,
  subtitle,
  successPageLink,
  url = '/api/contact-form',
  hiddenFields = {},
  submitLabel,
  successMessage,
}) {
  const router = useRouter();
  const getDefaultState = () =>
    fields
      .filter(({ type }) => type !== 'info')
      .reduce((prev, { name, defaultValue, type, fields: subfields }) => {
        if (type === 'double') {
          return {
            ...prev,
            ...subfields.reduce((prev, current) => {
              return { ...prev, [current.name]: current.defaultValue };
            }, {}),
          };
        }

        return {
          ...prev,
          [name]: defaultValue,
        };
      }, {});

  const [form, setForm] = useState(getDefaultState());
  const [formErrors, setFormErrors] = useState(null);
  const [success, setSuccess] = useState(false);
  const [vivus, setVivus] = useState(null);
  const [isInViewport, targetRef] = useIsInViewport({ threshold: 20 });

  useEffect(() => {
    setVivus(
      new Vivus(
        'contact-svg',
        {
          duration: 200,
          start: 'manual',
        },
      ),
    );
  }, []);

  if (isInViewport) {
    vivus.play();
  }

  const validateInput = (name, value) => {
    let field = fields.find(
      ({ name: fieldName, type: fieldType, fields: subfields }) => {
        if (fieldType === 'double') {
          const subfield = subfields.find(
            ({ name: subfieldName }) => subfieldName === name,
          );
          if (subfield) {
            return subfield;
          }
        }
        return name === fieldName;
      },
    );

    if (field.type === 'double') {
      field = field.fields.find(({ name: fieldName }) => fieldName === name);
    }

    return field.validationFn ? field.validationFn(value) : null;
  };

  const validateForm = () => {
    const formFields = Object.keys(form);
    let errors = {};
    formFields.forEach((fieldName) => {
      const value = form[fieldName];
      const error = validateInput(fieldName, value);
      if (error) {
        errors[fieldName] = error;
      }
    });

    errors = Object.keys(errors).length > 0 ? errors : null;
    setFormErrors(errors);

    return !errors;
  };

  const handleChange = ({ target }) => {
    const { name, value, type, checked, dataset, files } = target;
    const fieldDef = fields.find(
      ({ name: fieldName, type: fieldType, fields: subfields }) => {
        if (fieldType === 'double') {
          const subfield = subfields.find(
            ({ name: subfieldName }) => subfieldName === name,
          );
          if (subfield) {
            return subfield;
          }
        }
        return fieldName === name;
      },
    );
    let error;
    if (type === 'checkbox') {
      if (fieldDef && fieldDef.subtype === 'multi') {
        const subname = dataset ? dataset.subname : null;
        setForm({ ...form, [name]: { ...form[name], [subname]: checked } });
        error = validateInput(name, { ...form[name], [subname]: checked });
      } else {
        setForm({ ...form, [name]: checked });
        error = validateInput(name, checked);
      }
    } else if (type === 'file') {
      const file = files[0];
      setForm({ ...form, [name]: file });
      error = validateInput(name, file);
    } else {
      setForm({ ...form, [name]: value });
      error = validateInput(name, value);
    }

    setFormErrors({ ...formErrors, [name]: error });
  };

  const sendData = async (formData, sendUrl) => {
    const haveFileInput = !!fields.find(({ type }) => type === 'file');
    try {
      const res = await axios({
        method: 'post',
        url: sendUrl,
        headers: {
          Accept: 'application/json',
          'Content-Type': haveFileInput
            ? 'multipart/form-data'
            : 'application/json',
        },
        data: formData,
      });
      return res;
    } catch (error) {
      return error;
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = { ...form, ...hiddenFields };
    let formData;
    const hasFileInput = fields.find(({ type }) => type === 'file');
    if (hasFileInput) {
      formData = new FormData();
      for (let key in data) {
        if (typeof data[key] === 'object' && !data[key].size) {
          formData.append(key, JSON.stringify(data[key]));
        } else {
          formData.append(key, data[key]);
        }
      }
    } else {
      formData = data;
    }

    if (validateForm()) {
      const { data: result } = await sendData(formData, url);
      if (result && result.success) {
        setForm(getDefaultState());
        setFormErrors(null);
        if (successPageLink) {
          router.push(successPageLink.href, successPageLink.as);
        } else {
          setSuccess(true);
          setTimeout(() => {
            setSuccess(false);
          }, 5000);
        }
      } else {
        setFormErrors(result ? result.errors : undefined);
      }
    }
  };

  const renderFields = () => {
    return (
      <>
        {fields.map(({ type, subtype, name, fieldProps = {}, fields = [] }) => {
          const { required, label, rowsMax, content, subitems } = fieldProps;
          const fieldTypeClassName = `vr-form__field-box--${type}`;
          const fieldSubtypeClassName = `vr-form__field-box--${subtype}`;
          const formFieldClassNames = classNames('vr-form__field-box', {
            [fieldTypeClassName]: !!type,
            [fieldSubtypeClassName]: !!subtype,
          });
          return (
            <div className={formFieldClassNames} key={`form-field__${name}`}>
              {type === 'text' && (
                <div className="row">
                  <div className="col-xs-12 col-lg-8 col-lg-offset-2 col-xl-6 col-xl-offset-3">
                    <div className="vr-contact-form__field-box">
                      <TextField
                        className="vr-contact-form__field"
                        name={name}
                        id={name}
                        label={label}
                        value={form[name]}
                        required={required}
                        multiline={subtype === 'multiline'}
                        rowsMax={rowsMax}
                        error={formErrors && !!formErrors[name]}
                        helperText={formErrors ? formErrors[name] : ''}
                      />
                    </div>
                  </div>
                </div>
              )}

              {type === 'info' && content && (
                <div className="row">
                  <div className="col-xs-12 col-lg-8 col-lg-offset-2 col-xl-6 col-xl-offset-3">
                    <div className="vr-contact-form__field-box">
                      {content()}
                    </div>
                  </div>
                </div>
              )}

              {type === 'double' && (
                <div className="row">
                  <div className="col-xs-12 col-lg-8 col-lg-offset-2 col-xl-6 col-xl-offset-3">
                    <div className="vr-contact-form__field-box vr-contact-form__field-box--double">
                      {fields.map((field) => (
                        <TextField
                          className="vr-contact-form__field"
                          name={field.name}
                          id={field.name}
                          label={field.fieldProps.label}
                          value={form[field.name]}
                          required={field.fieldProps.required}
                          multiline={field.subtype === 'multiline'}
                          rowsMax={field.rowsMax}
                          error={formErrors && !!formErrors[field.name]}
                          helperText={formErrors ? formErrors[field.name] : ''}
                          key={field.name}
                        />
                      ))}
                    </div>
                  </div>
                </div>
              )}

              {type === 'checkbox' && !subtype && (
                <div className="row">
                  <div className="col-xs-12 col-lg-8 col-lg-offset-2">
                    <div className="vr-contact-form__field-box vr-contact-form__field-box--checkbox">
                      <FormControl
                        required
                        error={formErrors && !!formErrors[name]}
                      >
                        <VrCheckboxLabel
                          control={
                            <Checkbox
                              name={name}
                              value={form[name]}
                              checked={form[name]}
                            />
                          }
                          label={label}
                        />
                        <FormHelperText>
                          {formErrors ? formErrors[name] : null}
                        </FormHelperText>
                      </FormControl>
                    </div>
                  </div>
                </div>
              )}

              {type === 'checkbox' && subtype === 'multi' && (
                <div className="vr-contact-form__field-box vr-contact-form__field-box--multiline">
                  <FormControl
                    required
                    error={formErrors && !!formErrors[name]}
                  >
                    <FormLabel component="legend">{label}</FormLabel>
                    <FormGroup>
                      {subitems &&
                        subitems.map(({ name: subname, label: sublabel }) => (
                          <VrCheckboxLabel
                            key={`form-input__${name}-${subname}`}
                            control={
                              <Checkbox
                                name={name}
                                inputProps={{ 'data-subname': subname }}
                                value={form[name][subname]}
                                checked={form[name][subname]}
                              />
                            }
                            label={sublabel}
                          />
                        ))}
                    </FormGroup>
                    <FormHelperText>
                      {formErrors ? formErrors[name] : null}
                    </FormHelperText>
                  </FormControl>
                </div>
              )}
            </div>
          );
        })}
      </>
    );
  };

  return (
    <div className="vr-contact-form vr-section">
      <div className="vr-contact-form__svg" ref={targetRef}>
        <ContactSvg id="contact-svg" />
      </div>

      <div className="vr-contact-form__content">
        <VrSectionBanner title={title} subtitle={subtitle} inverted />

        <div className="container-fluid">
          <form
            autoComplete="off"
            className="vr-contact-form__form"
            noValidate
            onChange={handleChange}
            onSubmit={handleSubmit}
          >
            {renderFields()}

            <div className="vr-contact-form__field-box vr-contact-form__field-box--submit">
              {success ? (
                <h3>{successMessage}</h3>
              ) : (
                <VrButton type="submit">{submitLabel}</VrButton>
              )}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
