import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import React from 'react';

export const VrCheckboxLabel = withStyles({
  root: {
    alignItems: 'flex-start',
    color: '#fff'
  },
  label: {
    fontSize: '13px',
    lineHeight: '26px',
    fontWeight: '400',
    marginTop: '8px',
    color: '#fff'
  },
})((props) => <FormControlLabel {...props} />);
