import React, { useState } from 'react';
import { Carousel } from 'react-responsive-carousel';
import shortid from 'shortid';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import VrSectionBanner from './VrSectionBanner';
import VrFullWidthSection from './VrFullWidthSection';
import VrArrowButton from './VrArrowButton';

export default function VrShows({ title, subtitle, items }) {
  const [currentSlide, setCurrentSlide] = useState(0);
  const [isPlaying, setIsPlaying] = useState(true);

  const renderPrevArrow = (onClickHandler) => {
    return (
      <VrArrowButton
        onClick={onClickHandler}
        direction="left"
        className="vr-shows__arrow-button vr-shows__arrow-button--prev"
      />
    );
  };
  const renderNextArrow = (onClickHandler) => {
    return (
      <VrArrowButton
        onClick={onClickHandler}
        direction="right"
        className="vr-shows__arrow-button vr-shows__arrow-button--next"
      />
    );
  };

  const handleSlideChange = (index) => {
    setCurrentSlide(index);
  };

  const renderItems = () => items.map(({ title: itemTitle, description, image, link }, i) => (
      <div className={`vr-shows__item`} key={`${shortid.generate()}`}>
        <div className="vr-shows__child">
          <div className="row">
            <div className="col-xs-12 col-xl-8">
              <a href={link.url} title={itemTitle} className="vr-shows__child-image" tabIndex={-1}>
                <img src={image.url} alt={image.title} />
              </a>
            </div>
            <div className="col-xs-12 col-xl-4">
              <article className="vr-shows__child-content">
                <a href={link.url} title={itemTitle} tabIndex={currentSlide !== i ? -1 : 0}>
                  <h3>{itemTitle}</h3>
                </a>
                <a href={link.url} title={itemTitle} tabIndex={currentSlide !== i ? -1 : 0}>
                  {description && <div dangerouslySetInnerHTML={{ __html: description }} />}
                </a>
              </article>
            </div>
          </div>
        </div>
      </div>
    )
  );

  return (
    <div className="vr-shows vr-section">
      <VrSectionBanner title={title} subtitle={subtitle} inverted />
      <VrFullWidthSection>
        <div className="container-fluid">
          <div className="row center-xs justify">
            <div className="col-xs-12">
              <button className="vr-button play-pause-button" onClick={() => setIsPlaying(!isPlaying)}>
                {isPlaying ? <PauseIcon /> : <PlayArrowIcon />}
              </button>

              <Carousel
                showThumbs={false}
                interval={4000}
                transitionTime={500}
                infiniteLoop
                autoPlay={isPlaying}
                stopOnHover
                showIndicators={false}
                showStatus={false}
                renderArrowPrev={renderPrevArrow}
                renderArrowNext={renderNextArrow}
                onChange={handleSlideChange}
              >
                {renderItems()}
              </Carousel>
            </div>
          </div>
        </div>
      </VrFullWidthSection>
    </div>
  );
}
