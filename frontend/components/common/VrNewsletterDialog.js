import React, {useContext, useEffect, useState} from 'react';
import { motion } from 'framer-motion';
import CloseIcon from '@material-ui/icons/Close';
import VrNewsletterForm from './forms/VrNewsletterForm';
import { isEmail } from '../../utils/validators';
import ConfigContext from "../ConfigContext";

const CloseButton = ({ onClick, labels }) => {
  return (
    <button className="close-button" type="button" onClick={onClick}>
      <CloseIcon />
    </button>
  );
};

export default function VrNewsletterDialog({ close }) {
  let escKeypressListener;
  const config = useContext(ConfigContext);
  const isPl = config.locale === 'pl_PL';
  const labels = isPl ? {
    default: {
      first: 'Zapisz się na newsletter',
      second: 'Krakowski Teatr Variété',
    },
    success: {
      first: 'Do zobaczenia w Variété',
      second: 'Dziękujemy, sprawdź swoją skrzynkę',
    },
  } : {
    default: {
      first: 'Sign up to our newsletter',
      second: 'VARIETE THEATER',
    },
    success: {
      first: 'See You in Variété',
      second: 'Success, please check your mailbox',
    },
  };
  const checkboxLabel = isPl
      ? `* Wyrażam zgodę na używanie telekomunikacyjnych urządzeń końcowych i automatycznych systemów wywołujących (w tym między innymi skrzynki pocztowej e-mail, telefonów komórkowych) w celu prowadzenia za ich pomocą marketingu bezpośredniego przez Krakowski Teatr VARIETE z siedzibą w Krakowie, zgodnie z art. 172 ust. 1 ustawy z dnia 16 lipca 2004 r. Prawo telekomunikacyjne (Dz. U. 2019, poz. 2460, z późn. zm.).`
      : `* I hereby give my consent to the use of telecommunications terminal equipment and automatic calling systems (including but not limited to e-mail, mobile phones) for the purpose of direct marketing by the VARIETE Theatre in Krakow in compliance with Article 172(1) of the Act of 16 July 2004. - Telecommunication Law (Journal of Laws of 2019, item 2460, as amended).`;
  const fieldRequired = isPl ? 'To pole jest wymagane' : 'This field is required';
  const notEmail = isPl ? 'Podaj poprawny adres e-mail' : 'This is not valid e-mail address';
  const [success, setSuccess] = useState(false);
  const newsletterFormFields = [
    {
      defaultValue: '',
      type: 'text',
      name: 'name',
      fieldProps: {
        required: true,
        label: isPl ? 'Twoje imię i nazwisko' : 'Your name and surname',
      },
      validationFn(value) {

        return !value ? fieldRequired : null;
      },
    },
    {
      defaultValue: '',
      type: 'text',
      subtype: 'email',
      name: 'email',
      fieldProps: {
        required: true,
        label: isPl ? 'Twój e-mail' : 'Your e-mail address',
      },
      validationFn(value) {
        if (!value) {
          return fieldRequired;
        }
        if (!isEmail(value)) {
          return notEmail;
        }
        return null;
      },
    },
    {
      defaultValue: false,
      type: 'checkbox',
      name: 'rodo',
      fieldProps: {
        required: true,
        label: checkboxLabel
      },
      validationFn(value) {
        if (!value) {
          return fieldRequired;
        }
        return null;
      },
    },
  ];

  const closeDialog = () => {
    close();

    if (escKeypressListener) {
      window.removeEventListener('keydown', escKeypressListener);
    }
  };

  const successCb = () => setSuccess(true);

  useEffect(() => {
    escKeypressListener = window.addEventListener('keydown', ({ key }) => {
      if (key === 'Escape') {
        closeDialog();
      }
    });

    return () => {
      window.removeEventListener('keydown', escKeypressListener);
    }
  }, []);

  return (
    <motion.div
      className="vr-newsletter-dialog"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <div className="vr-newsletter-dialog__content">
        <CloseButton onClick={closeDialog} />

        <h2>{success ? labels.success.first : labels.default.first}</h2>
        <h3>{success ? labels.success.second : labels.default.second}</h3>
        {!success ? (
          <VrNewsletterForm
            fields={newsletterFormFields}
            submitLabel={isPl ? 'Zapisz się' : 'Subscribe'}
            successCallback={successCb}
          />
        ) : null}
      </div>
    </motion.div>
  );
}
