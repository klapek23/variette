import React from 'react';
import moment from 'moment';
import shortid from 'shortid';

import VrLink from './VrLink';

export default function VrNewsList({ title, items }) {
  return (
    <div className="vr-news-list vr-section vr-section--no-bottom-padding">
      <h3>{title}</h3>
      <div className="vr-news-list__list">
        {items.map(({ link, title, image, date }) => (
          <VrLink
            as={link.as}
            href={link.href}
            key={shortid.generate()}
            title={title.rendered}
            className="vr-news-list__list-item"
          >
            <div className="vr-news-list__list-item-image">
              <img src={image.url} alt={image.title || image.alt_text} />
            </div>
            <div className="vr-news-list__list-item-content">
              <div className="vr-news-list__list-item-title">
                <h4 dangerouslySetInnerHTML={{ __html: title }} />
              </div>
              <span className="vr-news-list__list-item-date">
                {moment(date).format('L')}
              </span>
            </div>
          </VrLink>
        ))}
      </div>
    </div>
  );
}
