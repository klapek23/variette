import React from 'react';
import shortid from 'shortid';
import VrSectionBanner from './VrSectionBanner';

const VrContactInfo = ({
  title,
  subtitle,
  titleInfo,
  address,
  ticketOffice,
}) => (
  <div className="vr-contact-info vr-section">
    <div className="vr-contact-info__bg">
      <div className="vr-contact-info__bg-item vr-contact-info__bg-item--left" />
      <div className="vr-contact-info__bg-item vr-contact-info__bg-item--right" />
    </div>
    <div className="container-fluid">
      <div className="row">
        <div className="col-xs-12">
          <div className="vr-contact-info__container">
            <VrSectionBanner
              title={title}
              subtitle={subtitle}
              info={titleInfo}
              main
            />

            <div className="row">
              <div className="col-xs-12 col-md-6 vr-contact-info__column vr-contact-info__column--address">
                <article>
                  <h3>{address.title}</h3>
                  <div dangerouslySetInnerHTML={{ __html: address.content }} />
                </article>
              </div>
              <div className="col-xs-12 col-md-6 vr-contact-info__column vr-contact-info__column--ticket-office">
                <h3>{ticketOffice.title}</h3>
                <ul className="vr-contact-info__list">
                  {ticketOffice.items.map(({ key, value }) => (
                    <li
                      key={shortid.generate()}
                      className="vr-contact-info__list-item"
                    >
                      <span className="vr-contact-info__list-item--key">
                        {key}
                      </span>
                      <span className="vr-contact-info__list-item--value">
                        {value}
                      </span>
                    </li>
                  ))}
                </ul>
                <div
                  dangerouslySetInnerHTML={{ __html: ticketOffice.footer }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default VrContactInfo;
