import React from 'react';
import classNames from 'classnames';
import { motion } from 'framer-motion';
import VrSectionBanner from './VrSectionBanner';
import VrNoResults from './VrNoResults';
import VrContentBoxesItem from './VrContentBoxesItem';

const VrContentBoxes = ({
  title,
  subtitle,
  items,
  bg,
  invertedHeader = false,
  main = false,
  white = false,
  className = '',
  gradientBottom = false,
}) => {
  const classnames = classNames('vr-section', 'vr-content-boxes', {
    'vr-content-boxes--white': white,
    'vr-content-boxes--gradient-bottom': gradientBottom,
  });
  return (
    <div
      className={`${classnames} ${className}`}
      style={bg && { backgroundImage: `url(${bg})` }}
    >
      {title && (
        <VrSectionBanner
          title={title}
          subtitle={subtitle || ''}
          inverted={invertedHeader}
          main={main}
        />
      )}
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12">
            <div className="vr-content-boxes__container">
              {items && items.length > 0 ? (
                items.map(
                  (
                    { slug, image, title: itemTitle, excerpt, link, bgText },
                    i,
                  ) => {
                    return (
                      <motion.div
                        initial={{ opacity: 0 }}
                        animate={{ opacity: 1 }}
                        transition={{ duration: 1 }}
                        className="vr-content-boxes__container-box"
                        key={`${slug}_${i}`}
                      >
                        <VrContentBoxesItem
                          title={itemTitle}
                          image={image}
                          excerpt={excerpt}
                          main={main}
                          link={link}
                          bgText={bgText}
                          buttonLabel={link.title}
                        />
                      </motion.div>
                    );
                  },
                )
              ) : (
                <VrNoResults />
              )}
            </div>
          </div>
        </div>
      </div>
      {gradientBottom && <div className="vr-content-boxes__bottom-gradient" />}
    </div>
  );
};

export default VrContentBoxes;
