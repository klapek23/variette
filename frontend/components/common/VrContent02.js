import React from 'react';
import VrSectionBanner from './VrSectionBanner';

const VrContent02 = ({children, title, subtitle = '', bg}) => {
  return (
    <div
      className="vr-content-02 vr-section"
      style={{
        backgroundImage: bg && bg.url ? `url(${bg.url})` : '',
        backgroundColor: bg && bg.color ? bg.color : '#fff',
      }}
    >
      <VrSectionBanner title={title} subtitle={subtitle} inverted/>
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12 col-xl-10 col-xl-offset-1">{children}</div>
        </div>
      </div>
    </div>
  )};

export default VrContent02;
