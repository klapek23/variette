import React from 'react';
import shortid from 'shortid';
import VrSectionBanner from './VrSectionBanner';
import VrLink from './VrLink';
import VrButton from './VrButton';

const VrTickets = ({ title, subtitle, titleInfo, items, button }) => {
  const renderItem = ({ title: itemTitle, description, image }) => {
    return (
      <div className="vr-tickets__steps-item" key={shortid.generate()}>
        <img src={image.url} alt={image.title} />
        <div>{itemTitle}</div>
        <p>{description}</p>
      </div>
    );
  };

  return (
    <div className="vr-tickets vr-section">
      <VrSectionBanner
        title={title}
        subtitle={subtitle}
        info={titleInfo}
        inverted
        main
      />
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12">
            <div className="vr-tickets__steps">
              {items && items.map((item) => renderItem(item))}
            </div>
            <div className="vr-tickets__button">
                <VrLink className="vr-button vr-button--black" as={button.url} title={button.title}>
                    {button.title}
                </VrLink>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VrTickets;
