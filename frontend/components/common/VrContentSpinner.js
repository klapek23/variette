import React from 'react';
import { AnimatePresence, motion } from 'framer-motion';

import SpinnerSvg from '../../static/spinner.svg';

export default function VrContentSpinner({ isVisible }) {
  return (
    <AnimatePresence>
      {isVisible && (
        <motion.div
          className="vr-content-spinner"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
        >
          <div className="vr-content-spinner__loader">
            <SpinnerSvg />
          </div>
          <div className="vr-content-spinner__bg" />
        </motion.div>
      )}
    </AnimatePresence>
  );
}
