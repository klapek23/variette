import React from 'react';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';

export default function VrNoResults({ label = 'No results found', icon = <SentimentVeryDissatisfiedIcon /> }) {
  return (
    <div className="vr-no-results">
      {icon} <span>{label}</span>
    </div>
  )
}
