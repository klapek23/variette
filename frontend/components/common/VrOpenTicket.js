import React from 'react';
import VrLink from "./VrLink";
import VrContent01 from "./VrContent01";

export default function VrOpenTicket({ title, subtitle, bg, content, link }) {

  return (
    <div className="vr-open-ticket" style={{ backgroundImage: `url(${bg})` }}>
      <VrContent01
        title={title}
        subtitle={subtitle}
      >
        <div dangerouslySetInnerHTML={{ __html: content }} />
        {link ? (
          <VrLink className="vr-button" as={link.url} title={link.title}>{link.title}</VrLink>
        ) : null}
      </VrContent01>
    </div>
  )
}
