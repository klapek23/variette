import React, { useState } from 'react';
import shortid from 'shortid';
import { useRouter } from "next/router";
import OutsideClickHandler from 'react-outside-click-handler';
import VrButton from './VrButton';
import VrLink from "./VrLink";
import {getCurrentLanguage, languages} from "../../utils/lang";

export default function VrLanguageSwitcher() {
  const router = useRouter();
  const [opened, setOpened] = useState(false);

  const handleClickedOutside = () => {
    setOpened(false);
  }

  const handleClick = () => {
    console.log('aaaaa')
    setOpened(!opened)
  }

  const currentLang = getCurrentLanguage(router.pathname);

  return <div className="vr-language-switcher">
    <OutsideClickHandler onOutsideClick={handleClickedOutside}>
      <VrButton icon onClick={handleClick}>{currentLang.label}</VrButton>
      <ul className={`${opened ? 'opened' : ''}`}>
        {languages.map(({ href, label }) => (
          <li key={shortid.generate()}>
            <VrLink title={label} as={href} href={href}>{label}</VrLink>
          </li>
        ))}
      </ul>
    </OutsideClickHandler>
  </div>;
}
