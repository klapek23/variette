import React, { useEffect, useState } from 'react';
import VrSectionBanner from './VrSectionBanner';
import useIsInViewport from 'use-is-in-viewport';
import Vivus from 'vivus';

const VrContent01Animated = ({
  children,
  title,
  subtitle,
  dark = false,
  image,
}) => {
  const [vivus, setVivus] = useState(null);
  const [isInViewport, targetRef] = useIsInViewport({ threshold: 30 });

  useEffect(() => {
    setVivus(
      new Vivus(
        image.id,
        {
          duration: 300,
          start: 'inViewport',
        },
        () => {
          const animatedElement = document.getElementById(image.id);
          if (animatedElement) {
            animatedElement.classList.add('ready');
          }
        },
      ),
    );
  }, []);

  if (isInViewport) {
    vivus.play();
  }

  return (
    <div
      className={`vr-content-01 vr-section ${dark && 'vr-content-01--dark'}`}
    >
      <div className="vr-content-01__cover" />
      <div className="vr-content-01__content">
        <VrSectionBanner
          title={title}
          subtitle={subtitle}
          inverted
          dark={!dark}
        />
        <div className="container-fluid">
          <div className="row middle-xs">
            <div className="col-xs-12 col-sm-12 col-lg-7">{children}</div>
            <div className="col-xs-12 col-sm-12 col-lg-5">
              <div className="abc" ref={targetRef}>
                <image.svg id={image.id} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VrContent01Animated;
