import React, { useEffect, useState } from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import useMouse from '@react-hook/mouse-position';
import useWindowSize from '../../utils/hooks/useWindowSize';

export default function VrFrontPage({ title, subtitle, buttonLabel, onClose }) {
  const [isVisible, setIsVisible] = useState(true);
  const windowSize = useWindowSize();
  const ref = React.useRef(null);

  let mouse;
  if (typeof window !== 'undefined') {
    mouse = useMouse(ref, {
      enterDelay: 100,
      leaveDelay: 100,
    });

    if (!mouse.x) {
      mouse = {
        clientX: window.innerWidth,
        clientY: window.innerHeight,
        elementHeight: window.innerHeight,
        elementWidth: window.innerWidth,
        pageX: window.innerWidth,
        pageY: window.innerHeight,
        screenX: window.innerWidth,
        screenY: window.innerHeight,
        x: window.innerWidth / 2,
        y: window.innerHeight / 2,
      };
    }
  }

  useEffect(() => {
    const enterEventListener = window.addEventListener('keydown', ({ key }) => {
      if (key === 'Enter') {
        setIsVisible(false);
      }
    });

    return () => {
      window.removeEventListener('keydown', enterEventListener);
    }
  }, []);

  const calculateRotateX = () => {
    return (mouse.elementHeight - mouse.clientY) / 20;
  };

  const calculateSkew = () => {
    const zero = mouse.elementWidth / 2;
    const y = (mouse.elementHeight - mouse.clientY) / 1000 + 0.1;
    return ((zero - mouse.x) / 100) * y;
  };

  const calculateScale = () => {
    const value = mouse.clientY / 500;
    return value < 1.2 ? 1.2 : value;
  };

  if (typeof document !== 'undefined') {
    if (isVisible) {
      document.body.style.overflowY = 'hidden';
    } else {
      document.body.style.overflowY = 'auto';
    }
  }

  const widthRatio = windowSize.width ? 1920 / windowSize.width : 1;
  const heightRatio = windowSize.height ? 1280 / windowSize.height : 1;

  return (
    <>
      <AnimatePresence>
        {isVisible && (
          <motion.div
            className="vr-front-page"
            initial={{
              translateY: isVisible ? '0%' : '-100%',
              opacity: isVisible ? 1 : 0,
            }}
            animate={{ translateY: '0%', opacity: 1 }}
            exit={{ translateY: '-100%', opacity: 0 }}
            transition={{ duration: 1 }}
            ref={ref}
          >
            {mouse && (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                style={{
                  width: '100%',
                  height: '100%',
                  position: 'absolute',
                  zIndex: 2,
                }}
                onClick={() => { setIsVisible(false); onClose() }}
                viewBox="0 0 1920 1280"
                preserveAspectRatio="xMinYMin slice"
              >
                <defs>
                  <radialGradient id="myGradient">
                    <stop offset="0%" stopColor="transparent" />
                    <stop offset="80%" stopColor="transparent" />
                    <stop offset="100%" stopColor="rgba(0,0,0,.7)" />
                  </radialGradient>
                  <clipPath
                    id="mask"
                    style={{
                      display: 'block',
                      width: '100%',
                      height: '100%',
                      position: 'absolute',
                    }}
                  >
                    <circle
                      id="mask-circle"
                      cx={0}
                      cy={0}
                      r="360px"
                      style={{
                        transformOrigin: 'center center',
                        transform: `rotateX(${calculateRotateX()}deg) skew(${calculateSkew()}deg) scale(${calculateScale()}) translateX(${mouse.x * widthRatio}px) translateY(${mouse.y * heightRatio}px)`,
                      }}
                    />
                  </clipPath>
                </defs>
                <g clipPath="url(#mask)">
                  <image
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                    xlinkHref="/static/intro.jpg"
                    style={{ height: '100%', minWidth: '100%' }}
                  />

                  <g className="txt" ctransform={`translate(${960 * widthRatio}, ${640 * heightRatio})`} preserveAspectRatio="xMidYMid meet" style={{ textAnchor: 'middle' }}>
                    <text className="txt-small">{title}</text>
                    <text className="txt-big">{subtitle}</text>

                    <g className="btn">
                      <rect className="btn-stroke" />
                      <text className="txt-btn">{buttonLabel}</text>
                    </g>
                  </g>

                  <circle
                    cx={0}
                    cy={0}
                    r="360px"
                    style={{
                      fill: "url('#myGradient')",
                      transformOrigin: 'center center',
                      transform: `rotateX(${calculateRotateX()}deg) skew(${calculateSkew()}deg) scale(${calculateScale()}) translateX(${mouse.x * widthRatio}px) translateY(${mouse.y * heightRatio}px)`,
                    }}
                  />
                </g>
              </svg>
            )}
            <div className="vr-front-page__cover" />
          </motion.div>
        )}
      </AnimatePresence>
    </>
  );
}
