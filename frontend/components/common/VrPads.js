import React from 'react';
import VrLink from "./VrLink";

const VrPads = ({ items, className = '' }) => (
  <div className={`vr-pads vr-section ${className}`}>
    <div className="container-fluid">
      <div className="row">
        <div className="col-xs-12">
          <div className="vr-pads__container">
            {items.map(({ title, as, href, label }, i) => (
              <VrLink className="vr-pads__item" as={as} href={href} title={title} key={`pad-item-${i}`}>
                <h3>{label}</h3>
                <span className="vr-pads__item-more">{title}</span>
              </VrLink>
            ))}
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default VrPads;
