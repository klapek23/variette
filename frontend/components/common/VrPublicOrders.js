import React from 'react';
import shortid from 'shortid';

const VrPublicOrders = ({ items }) => {
  const renderBlock = ({ title, items: blockItems }) => (
    <div className="vr-public-orders__block" key={shortid.generate()}>
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12 col-sm-12">
            <h3>{title}</h3>
            <ul className="vr-public-orders__list">
              {blockItems.map(({ label, link }) => (
                <li key={shortid.generate()}>{label} <strong><a href={link.url} title={link.title}>{link.title}</a></strong></li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <div className="vr-public-orders">
      {items.map((block) => renderBlock(block))}
    </div>
  )
};

export default VrPublicOrders;
