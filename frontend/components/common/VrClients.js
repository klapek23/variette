import React from 'react';
import Slider from 'react-slick';
import shortid from "shortid";

const VrClients = ({ items }) => {
  const responsive = [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: items.length > 5 ? 5 : items.length,
      },
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: items.length > 4 ? 4 : items.length,
      },
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: items.length > 3 ? 3 : items.length,
      },
    },
    {
      breakpoint: 575,
      settings: {
        slidesToShow: items.length > 2 ? 2 : items.length,
      },
    },
  ];
  return (
    <div className="vr-clients">
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12">
            <Slider
              responsive={responsive}
              dots={false}
              arrows={false}
              slidesToShow={items.length > 6 ? 6 : items.length}
              autoplaySpeed={4000}
              autoplay
            >
              {items.map(({ image, link }) =>
                link ? (
                  <a
                    href={link.url}
                    title={link.title}
                    key={shortid.generate()}
                    target="_blank"
                    rel="noreferrer"
                  >
                    <img src={image.url} alt={image.title} />
                  </a>
                ) : (
                  <img key={shortid.generate()} src={image.url} alt={image.title} />
                ),
              )}
            </Slider>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VrClients;
