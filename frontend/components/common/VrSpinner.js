import React from 'react';
import { motion, AnimatePresence } from "framer-motion"

import SpinnerSvg from '../../static/spinner.svg';

export default function VrSpinner({ isVisible }) {

  return (
    <AnimatePresence>
      {isVisible && <motion.div
        className="spinner-wrapper"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >

        <div className="loader">
          <SpinnerSvg />
        </div>

        <div className="spinner-bg" />
      </motion.div>}
    </AnimatePresence>
  )
}
