import React from 'react';
import shortid from 'shortid';
import VrLink from './VrLink';

function VrBreadcrumbs({ items, pageTitle }) {
  const createLinkContent = (as) => {
    const asArr = as.split('/').filter((item) => !!item);
    const asString = asArr[asArr.length - 1].replace(/-/g, ' ');
    const n = asString.indexOf('?');
    const value = n > 0 ? asString.substring(0, n) : asString;
    return value.charAt(0).toUpperCase() + value.slice(1);
  };

  const realItems = items.slice(0, items.length - 1);
  const lastItem = items[items.length - 1];

  return (
    <div className="vr-breadcrumbs">
      <ul>
        <li>
          <VrLink as="/" href="/">VARIETE</VrLink>
        </li>
        {realItems.map(({ as, href }) => (
          <li key={shortid.generate()}>
            <VrLink as={as} href={href}>
              <span dangerouslySetInnerHTML={{ __html: createLinkContent(as) }} />
            </VrLink>
          </li>
        ))}

        <li key={shortid.generate()}>
          <VrLink as={lastItem.as} href={lastItem.href}>
            <span dangerouslySetInnerHTML={{ __html: pageTitle }} />
          </VrLink>
        </li>
      </ul>
    </div>
  );
}

export default React.memo(VrBreadcrumbs);
