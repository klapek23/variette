import React from 'react';
import shortid from 'shortid';
import VrSectionBanner from './VrSectionBanner';

const VrEqualBoxes = ({ title, subtitle, items }) => {
  const renderItem = ({ title: itemTitle, content }) => {
    return (
      <div className="vr-equal-boxes__item" key={shortid.generate()}>
        <article>
          <h3>{itemTitle}</h3>
          <div dangerouslySetInnerHTML={{ __html: content }} />
        </article>
      </div>
    );
  };

  return (
    <div
      className="vr-equal-boxes vr-section"
      style={{ background: '#f1f1f1' }}
    >
      <VrSectionBanner title={title} subtitle={subtitle} inverted dark />
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12">
            <div className="vr-equal-boxes__container">
              {items && items.map((item) => renderItem(item))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VrEqualBoxes;
