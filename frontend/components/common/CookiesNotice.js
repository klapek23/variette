import React, { useState } from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import useLocalStorageState from '../../utils/hooks/useLocalStorageState';
import {VrCheckboxLabel} from "./forms/VrCheckbox";
import VrButton from "./VrButton";

const CloseButton = ({ onClick, label = 'Zamknij' }) => (
  <VrButton small className="cookiesNotice__button-close" onClick={onClick}>
    <span>{label}</span>
  </VrButton>
);

export default function CookiesNotice({
  message,
  display_close_button: displayCloseButton,
  cookie_info_placement: placement,
  labels,
}) {
  const key = 'display_cookies';
  const googleAnalyticsKey = 'display_ga';
  const googleMapsKey = 'display_gmaps';
  const youtubeKey = 'display_youtube';

  const [hide, setHide] = useState(false);
  const [display, setDisplay] = useLocalStorageState(true, key);
  const [displayGmaps, setDisplayGmaps] = useLocalStorageState(false, googleMapsKey);
  const [displayYoutube, setDisplayYoutube] = useLocalStorageState(false, youtubeKey);
  const [displayGA, setDisplayGA] = useLocalStorageState(false, googleAnalyticsKey);

  const closeCookies = () => {
    setDisplay(false);
    window.location.reload(false);
  };

  // const display = !displayYoutube || !displayGmaps;

  return (
    <div className={`cookiesNotice cookiesNotice--${placement}`}>
      <AnimatePresence>
        {display ? (
          <motion.div
            initial={{ opacity: 0, height: 0 }}
            animate={{ opacity: 1, height: 'auto' }}
            exit={{ opacity: 0, height: 0 }}
          >
            <div className="cookiesNotice__content">
              <p>{message}</p>
              <div className="cookiesNotice__content-section">
                <VrCheckboxLabel
                  label="Google Maps cookies"
                  control={
                    <Checkbox name="gmapsCookies" value={displayGmaps} checked={displayGmaps} onChange={() => setDisplayGmaps(!displayGmaps)} />
                  }
                />
                <VrCheckboxLabel
                  label="Google Analytics cookies"
                  control={
                    <Checkbox name="gaCookies" value={displayGA} checked={displayGA} onChange={() => setDisplayGA(!displayGA)} />
                  }
                />
                <VrCheckboxLabel
                  label="Youtube cookies"
                  control={
                    <Checkbox name="youtubeCookies" value={displayYoutube} checked={displayYoutube} onChange={() => setDisplayYoutube(!displayYoutube)} />
                  }
                />
              </div>
            </div>

            {displayCloseButton === '1' && (
              <CloseButton label={labels.close} onClick={closeCookies} />
            )}
          </motion.div>
        ) : null}
      </AnimatePresence>
    </div>
  );
}
