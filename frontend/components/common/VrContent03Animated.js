import React, { useEffect, useState } from 'react';
import useIsInViewport from 'use-is-in-viewport';
import classNames from 'classnames';
import Vivus from 'vivus';

import VrSectionBanner from './VrSectionBanner';
import VrFullWidthSection from './VrFullWidthSection';

const VrContent03Animated = ({
  children,
  title,
  subtitle,
  image,
  bgColor = '#fff',
  inverted = false,
  equalColumns = false,
  white = false,
  className = ''
}) => {
  const classnames = classNames('vr-content-03--animated', 'vr-section', {
    'vr-content-03--inverted': inverted,
    'vr-content-03--equal-columns': equalColumns,
    'vr-content-03--white': white,
    [`vr-content-03--${className}`]: !!className
  });

  const [vivus, setVivus] = useState(null);
  const [isInViewport, targetRef] = useIsInViewport({ threshold: 30 });

  useEffect(() => {
    setVivus(
      new Vivus(
        image.id,
        {
          duration: 300,
          start: 'inViewport',
        },
        () => {
          const animatedElement = document.getElementById(image.id);
          if (animatedElement) {
            animatedElement.classList.add('ready');
          }
        },
      ),
    );
  }, []);

  if (isInViewport) {
    vivus.play();
  }

  return (
    <div className={classnames} style={{ backgroundColor: bgColor }}>
      <VrSectionBanner
        title={title}
        subtitle={subtitle}
        dark={!white}
        inverted
      />
      <VrFullWidthSection>
        <div className="container-fluid">
          <div className="row middle-xs vr-content-03__container">
            {inverted ? (
              <>
                <div
                  className={
                    !equalColumns ? `col-xs-12 col-lg-5 vr-content-03__anim-box` : `col-xs-12 col-lg-6 vr-content-03__anim-box`
                  }
                >
                  <div className="animation" ref={targetRef}>
                    <image.svg id={image.id} />
                  </div>
                </div>
                <div
                  className={
                    !equalColumns ? `col-xs-12 col-lg-7` : 'col-xs-12 col-lg-6'
                  }
                >
                  {children}
                </div>
              </>
            ) : (
              <>
                <div
                  className={
                    !equalColumns ? `col-xs-12 col-lg-7` : 'col-xs-12 col-lg-6'
                  }
                >
                  {children}
                </div>
                <div
                  className={
                    !equalColumns
                      ? `col-xs-12 col-lg-5 first-xs last-lg vr-content-03__anim-box`
                      : `col-xs-12 col-lg-6 first-xs last-lg vr-content-03__anim-box`
                  }
                >
                    <div className="animation" ref={targetRef}>
                      <image.svg id={image.id} />
                    </div>
                </div>
              </>
            )}
          </div>
        </div>
      </VrFullWidthSection>
    </div>
  );
};

export default VrContent03Animated;
