import React from 'react';
import VrSectionBanner from "./VrSectionBanner";
import VrFullWidthSection from "./VrFullWidthSection";
import VrLink from "./VrLink";

export default function VrRepeatableSection({ title, image, inverted, children, link }) {
  return (
    <div
      className={`vr-repeatable-section vr-section ${inverted ? 'vr-repeatable-section--inverted' : ''}`}
    >
      <VrFullWidthSection>
        {title && <VrSectionBanner title={title} />}
        <div className="container-fluid">
          <div className="row">
            {inverted
              ? <>
                <div className="col-xs-12 col-lg-6">
                  <div className='vr-repeatable-section__image'>
                    {link ? <VrLink {...link} tabIndex={-1}>
                      <img src={image.url} alt={image.alt || image.title} />
                    </VrLink> : <img src={image.url} alt={image.alt || image.title} />
                    }
                  </div>
                </div>
                <div className="col-xs-12 col-lg-5">
                  {children}
                </div>
              </>
              : <>
                <div className="col-xs-12 col-lg-5 col-lg-offset-1">
                  {children}
                </div>
                <div className="col-xs-12 col-lg-6 first-xs last-lg">
                  <div className='vr-repeatable-section__image'>
                    {link ? <VrLink {...link} tabIndex={-1}>
                      <img src={image.url} alt={image.alt || image.title}/>
                    </VrLink> : <img src={image.url} alt={image.alt || image.title} />
                    }
                  </div>
                </div>
              </>
            }
          </div>
        </div>
      </VrFullWidthSection>
    </div>
  )
}
