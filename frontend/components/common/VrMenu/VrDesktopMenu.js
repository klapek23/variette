import React from 'react';
import { useRouter} from 'next/router';
import classNames from 'classnames';

import {ConfigConsumer} from "../../ConfigContext";
import VrLink from '../VrLink';
import VrButton from '../VrButton';

export default function VrDesktopMenu({menu}) {
  const router = useRouter();
  const {asPath} = router;

  const renderItems = (config) => {
    const pathnameArr = asPath.split('/');
    const currentTopLevelItem = pathnameArr[1];
    const currentSecondLevelItem = `${pathnameArr[1]}/${pathnameArr[2] || ''}`;

    return menu.items.map((item) => {
      const regexp = new RegExp(`^${config.wpUrl}`, 'gmi');
      const parsedItemUrl = item.url.replace(regexp, '').replace(/\/$/gim, '');
      const hasActiveChild = !!(
        item.child_items &&
        item.child_items.find(({url}) => {
          let parsedUrl = url.split('/');
          parsedUrl = parsedUrl
            .slice(Math.max(parsedUrl.length - 3, 1))
            .join('/')
            .replace(/\/$/gim, '');
          return currentSecondLevelItem === parsedUrl;
        })
      );
      const itemClassnames = classNames('header-menu__item', {
        special: item.classes.includes('special'),
        'has-submenu': item.child_items,
      });

      return (
        <li className={itemClassnames} key={item.ID}>
          <div className="header-menu__item-content">
            {item.url === '#' ? (
              <span
                className={`${
                  '/' + currentTopLevelItem === parsedItemUrl || hasActiveChild
                    ? 'active'
                    : ''
                }`}
              >
              {item.title}
            </span>
            ) : item.classes.includes('special') ? (
              <VrLink
                as={parsedItemUrl}
                href={parsedItemUrl}
                title={item.attr_title || item.title}
                key={item.ID}
                className="special vr-button"
              >
                {item.title}
              </VrLink>
            ) : (
              <VrLink
                as={parsedItemUrl}
                href={parsedItemUrl}
                title={item.attr_title || item.title}
                key={item.ID}
                className={`${
                  '/' + currentTopLevelItem === parsedItemUrl ? 'active' : ''
                }`}
              >
                {item.title}
              </VrLink>
            )}
          </div>

          {item.child_items && (
            <div className="header-menu__submenu">
              <ul>
                {item.child_items.map((subitem) => {
                  const parsedSubitemUrl = subitem.url
                    .replace(regexp, '')
                    .replace(/\/$/gim, '');
                  const repertoireSlugs = [
                    'koncerty',
                    'spektakle',
                    'goscinne',
                    'wydarzenia',
                    'archiwum',
                  ];
                  const enRepertoireSlugs = [
                    'concerts',
                    'shows',
                    'guest-shows',
                    'events',
                    'archive',
                  ];
                  let subitemHref = parsedSubitemUrl;
                  if (repertoireSlugs.includes(subitem.slug)) {
                    subitemHref = '/repertuar/[type]';
                  } else if(enRepertoireSlugs.includes(subitem.slug)) {
                    subitemHref = '/repertoir/[type]';
                  }

                  return (
                    <li
                      key={subitem.ID}
                      className={`${
                        '/' + currentSecondLevelItem === parsedSubitemUrl
                          ? 'active'
                          : ''
                      }`}
                    >
                      <VrLink href={subitemHref} as={parsedSubitemUrl} title={subitem.attr_title || subitem.title}>
                        {subitem.title}
                      </VrLink>
                    </li>
                  );
                })}
              </ul>
            </div>
          )}
        </li>
      );
    });
  };

  return <ConfigConsumer>
    {(config) => (
      <ul className={`header-menu__items`}>{renderItems(config)}</ul>
    )}
  </ConfigConsumer>
}
