import React from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {useRouter} from 'next/router';
import {AnimatePresence, motion} from 'framer-motion';

import {ConfigConsumer} from "../../ConfigContext";
import VrLink from '../VrLink';
import VrButton from '../VrButton';

const VrMobileMenu = React.memo(({isMenuActive, menu}) => {
  const router = useRouter();
  const {asPath} = router;

  const toggleSubmenu = (e) => {
    const submenu = e.currentTarget.parentNode.parentNode.childNodes[1];
    const link = e.currentTarget;
    if (submenu.classList.contains('open')) {
      submenu.classList.remove('open');
      link.classList.remove('open');
    } else {
      submenu.classList.add('open');
      link.classList.add('open');
    }
  };

  const renderItems = (config) => {
    const pathnameArr = asPath.split('/');
    const currentTopLevelItem = pathnameArr[1];
    const currentSecondLevelItem = `${pathnameArr[1]}/${pathnameArr[2]}`;

    return menu.items.map((item) => {
      const regexp = new RegExp(`^${config.wpUrl}`, 'gmi');
      const parsedItemUrl = item.url.replace(regexp, '').replace(/\/$/gim, '');
      const hasActiveChild = !!(
        item.child_items &&
        item.child_items.find(({url}) => {
          let parsedUrl = url.split('/');
          parsedUrl = parsedUrl
            .slice(Math.max(parsedUrl.length - 3, 1))
            .join('/')
            .replace(/\/$/gim, '');
          return currentSecondLevelItem === parsedUrl;
        })
      );

      return (
        <div
          className={`header-menu__item ${
            item.child_items ? 'has-submenu' : ''
          }`}
          key={item.ID}
        >
          <div className="header-menu__item-content">
            {item.url === '#' ? (
              <div
                className={`${hasActiveChild ? 'active' : ''}`}
                onClick={toggleSubmenu}
              >
                <span
                  className={`${
                    '/' + currentTopLevelItem === parsedItemUrl ||
                    hasActiveChild
                      ? 'active'
                      : ''
                  }`}
                >
                  {item.title}
                </span>
                <ExpandMoreIcon/>
              </div>
            ) : item.classes.includes('special') ? (
              <VrLink
                as={parsedItemUrl}
                href={parsedItemUrl}
                key={item.ID}
                title={item.attr_title || item.title}
                className="special vr-button"
              >
                {item.title}
              </VrLink>
            ) : (
              <VrLink
                as={parsedItemUrl}
                href={parsedItemUrl}
                key={item.ID}
                title={item.attr_title || item.title}
                className={`${
                  '/' + currentTopLevelItem === parsedItemUrl ? 'active' : ''
                }`}
              >
                {item.title}
              </VrLink>
            )}
          </div>

          {item.child_items && (
            <div className="header-menu__submenu">
              <ul>
                {item.child_items.map((subitem) => {
                  const parsedSubitemUrl = subitem.url
                    .replace(regexp, '')
                    .replace(/\/$/gim, '');
                  const repertoireSlugs = [
                    'koncerty',
                    'spektakle',
                    'goscinne',
                    'wydarzenia',
                  ];
                  const enRepertoireSlugs = [
                    'concerts',
                    'shows',
                    'guest-shows',
                    'events',
                    'archive',
                  ];
                  let subitemHref = parsedSubitemUrl;
                  if (repertoireSlugs.includes(subitem.slug)) {
                    subitemHref = '/repertuar/[type]';
                  } else if(enRepertoireSlugs.includes(subitem.slug)) {
                    subitemHref = '/repertoir/[type]';
                  }

                  return (
                    <li
                      key={subitem.ID}
                      className={`${
                        '/' + currentSecondLevelItem === parsedSubitemUrl
                          ? 'active'
                          : ''
                      }`}
                    >
                      <VrLink href={subitemHref} as={parsedSubitemUrl} title={subitem.attr_title || subitem.title}>
                        {subitem.title}
                      </VrLink>
                    </li>
                  );
                })}
              </ul>
            </div>
          )}
        </div>
      );
    });
  };

  return (
    <ConfigConsumer>
      {(config) => (
        <AnimatePresence>
          {isMenuActive && (
            <motion.div
              className="mobile-menu"
              initial={{opacity: 0, translateX: '100%'}}
              animate={{opacity: 1, translateX: '0%'}}
              exit={{opacity: 0, translateX: '100%'}}
              transition={{duration: 0.5}}
            >
              <div className="mobile-menu__items">{renderItems(config)}</div>
            </motion.div>
          )}
        </AnimatePresence>
      )}
    </ConfigConsumer>
  );
});

export default VrMobileMenu;
