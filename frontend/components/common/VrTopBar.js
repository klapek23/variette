import React, {useState} from 'react';
import {AnimatePresence} from 'framer-motion';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import YouTubeIcon from '@material-ui/icons/YouTube';
import EmailIcon from '@material-ui/icons/Email';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import AccessibleIcon from '@material-ui/icons/Accessible';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import OutsideClickHandler from 'react-outside-click-handler';
import {IconButton} from '@material-ui/core';

import VrButton from './VrButton';
import VrLink from './VrLink';
import VrLanguageSwitcher from './VrLanguageSwitcher';
import VrDialogPortal from './VrDialogPortal';
import VrNewsletterDialog from './VrNewsletterDialog';
import {ThemeOptionsConsumer} from '../ThemeOptionsContext';

const VrTopBar = ({socials, themeOptions}) => {
  const [opened, setOpened] = useState(false);
  const [hc, setHc] = useState(false);
  const [fontSize, setFontSize] = useState(1);
  const [dialogOpened, setDialogOpened] = useState(false);

  const toggleOpened = () => {
    setOpened(!opened);
  };

  const handleChangeHC = () => {
    const classlist = document.body.classList;
    const classname = 'high-contrast';
    if (!hc) {
      classlist.add(classname);
    } else {
      classlist.remove(classname);
    }
    setHc(!hc);
  };

  const handleChangeFont = (size) => {
    const body = document.body;
    if (fontSize !== 1) {
      body.className = body.className.replace(/font-x.*/gim, '');
    }
    if (size !== 1) {
      body.classList.add(`font-x${size}`);
    }
    setFontSize(size);
  };

  const calculateBiggerFont = () => (fontSize < 3 ? fontSize + 1 : 1);

  const closeIconsDropdown = () => setOpened(false);

  const toggleDialog = () => {
    if (dialogOpened) {
      document.body.style.overflowY = 'auto';
    } else {
      document.body.style.overflowY = 'hidden';
    }
    setDialogOpened(!dialogOpened);
  };

  return (
        <div className="vr-top-bar">
          <AnimatePresence>
            {dialogOpened ? (
              <VrDialogPortal>
                <VrNewsletterDialog close={toggleDialog}/>
              </VrDialogPortal>
            ) : null}
          </AnimatePresence>
          <div className="vr-top-bar__newsletter vr-top-bar__set">
            <VrButton color="white" small onClick={toggleDialog}>
              Newsletter
            </VrButton>
            <IconButton onClick={toggleDialog}>
              <EmailIcon/>
            </IconButton>
          </div>
          <OutsideClickHandler onOutsideClick={closeIconsDropdown}>
            <div
              className={`vr-top-bar__menu ${
                opened ? 'vr-top-bar__menu--opened' : ''
              } vr-top-bar__set`}
            >
              <div className="vr-top-bar__socials vr-top-bar__set">
                <VrLink as={socials.facebookUrl} title="Facebook">
                  <FacebookIcon/>
                </VrLink>
                <VrLink as={socials.instagramUrl} title="Instagram">
                  <InstagramIcon/>
                </VrLink>
                <VrLink as={socials.youtubeUrl} title="Youtube">
                  <YouTubeIcon/>
                </VrLink>
              </div>
              <div className="vr-top-bar__accessibility vr-top-bar__set">
                <IconButton
                  aria-label="toggle-high-contrast"
                  onClick={handleChangeHC}
                >
                  {hc ? <VisibilityOffIcon/> : <VisibilityIcon/>}
                </IconButton>
                <IconButton
                  aria-label="change-font-size"
                  className="vr-top-bar__accessibility-font-container vr-button"
                  onClick={() => handleChangeFont(calculateBiggerFont())}
                >
                  <em className="vr-top-bar__accessibility-font vr-top-bar__accessibility-font--small">
                    A
                  </em>
                  <em className="vr-top-bar__accessibility-font vr-top-bar__accessibility-font--medium">
                    A
                  </em>
                  <em className="vr-top-bar__accessibility-font vr-top-bar__accessibility-font--big">
                    A
                  </em>
                </IconButton>
                  <VrLink
                    as={themeOptions.accessibleLink}
                    href={themeOptions.accessibleLink}
                    title="Accessibility"
                    className="MuiButtonBase-root"
                  >
                    <AccessibleIcon/>
                  </VrLink>
              </div>
            </div>
            <IconButton
              aria-label="toggle-dropdown"
              className="vr-top-bar__more vr-button"
              onClick={toggleOpened}
            >
              <MoreVertIcon/>
            </IconButton>
          </OutsideClickHandler>
          <VrLanguageSwitcher/>
        </div>
  );
};

export default (props) => (
  <ThemeOptionsConsumer>
    {(themeOptions) => <VrTopBar themeOptions={themeOptions} {...props}  />}
  </ThemeOptionsConsumer>
);
