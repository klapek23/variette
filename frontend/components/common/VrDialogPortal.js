import React from 'react';
import ReactDOM from 'react-dom';

export default function VrDialogPortal({ children }) {
  const dom = document.getElementById('vr-portal');

  return ReactDOM.createPortal(children, dom);
};
