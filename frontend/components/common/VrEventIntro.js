import React from 'react';
import VrSectionBanner from './VrSectionBanner';

const VrEventIntro = ({ title, subtitle, image }) => {
  return (
    <div className="vr-event-intro vr-section">
      <VrSectionBanner title={title} subtitle={subtitle} inverted main />
      <div className="vr-event-intro__image">
        <img src={image.url} alt={image.title} />
      </div>
    </div>
  );
};

export default VrEventIntro;
