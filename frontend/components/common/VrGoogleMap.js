import React, { useEffect } from 'react';
import useLocalStorageState from "../../utils/hooks/useLocalStorageState";

export default function VrGoogleMap({ latLng, map }) {
  const [displayGmaps] = useLocalStorageState(false, 'display_gmaps');

  useEffect(() => {
    if (displayGmaps) {
      const gmap = new google.maps.Map(document.getElementById("gmap"), {
        mapId: "8bb4b38fcb9dc41c",
        center: latLng,
        zoom: 16,
      });

      const marker = new google.maps.Marker({
        position: latLng,
        map: gmap,
        icon: '/static/maps-marker.png',
        title: map.address,
      });
      marker.addListener("click", toggleBounce);

      function toggleBounce() {
        if (marker.getAnimation() !== null) {
          marker.setAnimation(null);
        } else {
          marker.setAnimation(google.maps.Animation.BOUNCE);
          const infowindow = new google.maps.InfoWindow({
            content: map.address,
            position: latLng,
          });
          // infowindow.open(gmap);
        }
      }
    }
  }, []);

  return displayGmaps && <div id="gmap" />;
}