import React from 'react';
import VrShowCreators from './components/VrShowCreators';
import VrShowTickets from './components/VrShowTickets';
import VrShowUpcomingShows from './components/VrShowUpcomingShows';

const VrShowInfo = ({ creators, tickets, upcoming }) => {
  return (
    <div className="vr-show-info">
      <div className="vr-show-info__section">
        {creators && (
          <VrShowCreators title={creators.title} creators={creators.items} />
        )}
      </div>
      <div className="vr-show-info__section">
        {tickets && (
          <VrShowTickets title={tickets.title} tickets={tickets.items} />
        )}
      </div>
      <div className="vr-show-info__section">
        {upcoming && (
          <VrShowUpcomingShows title={upcoming.title} shows={upcoming.items} notFound={upcoming.not_found} />
        )}
      </div>
    </div>
  );
};

export default VrShowInfo;
