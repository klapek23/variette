import React from 'react';
import shortid from "shortid";
import ConfirmationNumberOutlinedIcon from '@material-ui/icons/ConfirmationNumberOutlined';
import LocalActivityOutlinedIcon from '@material-ui/icons/LocalActivityOutlined';

export default function VrShowTickets({ title, tickets }) {
  return (
    <>
      <h3>{title}</h3>
      <article>
        <ul>
          {tickets.map(({ discount, label }) => (
            <li key={shortid.generate()}>
              {discount ? <ConfirmationNumberOutlinedIcon /> : <LocalActivityOutlinedIcon />}
              {label}
            </li>
          ))}
        </ul>
      </article>
    </>
  )
}
