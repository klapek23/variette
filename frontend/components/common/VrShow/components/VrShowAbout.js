import React from 'react';

export default function VrShowAbout({ content }) {
  return (
    <article
      className="vr-show-details__about"
      dangerouslySetInnerHTML={{ __html: content }}
    />
  );
}
