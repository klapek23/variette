import React, { useState, useEffect } from 'react';
import shortid from 'shortid';
import { motion, AnimatePresence } from 'framer-motion';

const itemAnimateProps = {
  initial: {
    height: 0,
    opacity: 0,
  },
  animate: {
    height: 'auto',
    opacity: 1,
  },
  exit: {
    height: 0,
    opacity: 0,
  },
};

export default function VrShowReviews({ reviews, labels }) {
  const Review = ({ header, excerpt, content }) => {
    const [opened, setOpened] = useState(false);
    const handleMoreClick = () => {
      setOpened(!opened);
    };
    return (
      <section className="vr-show-details__reviews-item-box">
        <h5 className="vr-show-details__reviews-item-header">{header}</h5>
        <p
          dangerouslySetInnerHTML={{ __html: excerpt }}
          className="vr-show-details__reviews-excerpt"
        />
        {!opened ? (
          <button
            type="button"
            className="vr-show-details__reviews-more"
            onClick={handleMoreClick}
          >
            {labels.readMore}
          </button>
        ) : null}
        <AnimatePresence>
          {opened ? (
            <motion.div
              className="vr-show-details__reviews-item-content"
              dangerouslySetInnerHTML={{ __html: content }}
              {...itemAnimateProps}
            />
          ) : null}
        </AnimatePresence>
        {opened ? (
          <button
            type="button"
            className="vr-show-details__reviews-less"
            onClick={handleMoreClick}
          >
            {labels.collapse}
          </button>
        ) : null}
      </section>
    );
  };

  return (
    <>
      <article>
        <ul className="vr-show-details__reviews">
          {reviews.map(({ header, excerpt, content }) => (
            <li
              key={shortid.generate()}
              className="vr-show-details__reviews-item"
            >
              <Review header={header} excerpt={excerpt} content={content} />
            </li>
          ))}
        </ul>
      </article>
    </>
  );
}
