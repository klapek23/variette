import React from 'react';
import shortid from "shortid";
import moment from "moment";
import VrLink from "../../VrLink";
import VrButton from "../../VrButton";

export default function VrShowUpcomingShows({ title, shows, notFound }) {
  const sorted = shows ? shows.sort((a, b) => {
    const aDate = moment(a.date);
    const bDate = moment(b.date);
    if (aDate > bDate) {
      return 1;
    } else {
      return -1;
    }
  }) : [];
  return (
    <>
      <h3>{title}</h3>
      <article>
        {sorted.length > 0
          ? (
            <ul>
              {sorted.map(({ date, link }) => {
                return (
                  <li key={shortid.generate()}>{moment(date).format('DD.MM.YYYY')} | {moment(date).format('dd')} | {moment(date).format('HH:mm')}
                    <VrLink as={link.url} title={link.title}><VrButton smallWithBigText>{link.title}</VrButton></VrLink>
                  </li>
                )})}
            </ul>
          )
          : (
            <p>{notFound}</p>
          )
        }
      </article>
    </>
  )
}
