import React from 'react';
import shortid from 'shortid';

export default function VrShowActors({ actors }) {
  return (
    <div className="vr-show-details__actors">
      {actors.map(({ image, name, role }) => (
        <div className="vr-show-details__actors-item" key={shortid.generate()}>
          <div className="vr-show-details__actors-item-image">
            <img src={image.url} alt={image.title} />
          </div>
          <h4>{name}</h4>
          <span>{role}</span>
        </div>
      ))}
    </div>
  );
}
