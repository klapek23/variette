import React from 'react';
import VrPlayer from '../../VrPlayer';

export default function VrShowVideo({url, poster}) {
  return <VrPlayer url={url} poster={poster}/>;
}
