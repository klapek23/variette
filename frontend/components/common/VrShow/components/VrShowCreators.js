import React from 'react';
import shortid from 'shortid';

export default function VrShowCreators({ title, creators }) {
  return (
    <>
      <h3>{title}</h3>
      <article>
        <ul>
          {creators.map(({ role, name }) => (
            <li key={shortid.generate()}>
              <span>{role}:</span> <strong>{name}</strong>
            </li>
          ))}
        </ul>
      </article>
    </>
  );
}
