import React  from 'react';

export default function VrShowCover({ image }) {
  return (
    <article>
      <img
        src={image.url}
        alt={image.title}
        className="vr-show-details__cover"
      />
    </article>
  );
}
