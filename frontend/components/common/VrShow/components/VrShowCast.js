import React from 'react';
import shortid from 'shortid';
import VrSelect from '../../VrSelect';

export default function VrShowCast({
  cast,
  currentCastDate,
  onCastDateChange,
  labels,
}) {
  const handleCastDateChange = (item) => {
    onCastDateChange(item);
  };

  return (
    <div className="vr-show-details__cast">
      <div className="vr-show-details__cast-header">
        <h4>{labels.check}</h4>
        <VrSelect
          items={cast}
          currentItem={currentCastDate}
          onChange={handleCastDateChange}
        />
      </div>
      <div className="vr-show-details__cast-items">
        <ul className="vr-show-details__cast-list">
          {currentCastDate.items && currentCastDate.items.map(({ name, role }) => (
            <li key={shortid.generate()}>
              <strong>{role}</strong>
              <span>{name}</span>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}
