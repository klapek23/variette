import React, { Component } from 'react';
import Slider from "react-slick";
import shortid from 'shortid';
import VrSectionBanner from "../VrSectionBanner";
import VrArrowButton from "../VrArrowButton";
import debounce from '../../../utils/debounce';

const PrevArrow = ({ onClick }) => {
  return (
    <VrArrowButton direction="left" className="vr-show-photos__arrow-button vr-show-photos__arrow-button--prev" onClick={onClick} />
  )
}
const NextArrow = ({ onClick }) => {
  return (
    <VrArrowButton direction="right" className="vr-show-photos__arrow-button vr-show-photos__arrow-button--next" onClick={onClick} />
  )
}

export default class VrShowPhotos extends Component {
  sliderSettings = {
    centerMode: true,
    infinite: false,
    centerPadding: "200px",
    slidesToShow: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          centerPadding: "150px"
        }
      },
      {
        breakpoint: 991,
        settings: {
          centerPadding: "100px"
        }
      },
      {
        breakpoint: 767,
        settings: {
          centerPadding: "80px"
        }
      },
      {
        breakpoint: 499,
        settings: {
          centerPadding: "50px"
        }
      }
    ]
  }

  thumbsSettings = {
    slidesToShow: 6,
    slidesToScroll: 1,
    infinite: false,
    swipeToSlide: true,
    focusOnSelect: false,
    prevArrow: <PrevArrow />,
    nextArrow: <NextArrow />,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 5,
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 420,
        settings: {
          slidesToShow: 2,
        }
      },
    ]
  }

  constructor(props) {
    super(props);
    this.state = {
      sliderRef: null,
      thumbsRef: null,
      currentActive: 0,
    };
  }

  componentDidMount() {
    this.setState({
      sliderRef: this.sliderRef,
      thumbsRef: this.thumbsRef
    });
  }

  handleThumbClick(i) {
    const { sliderRef } = this.state;
    this.setState({ currentActive: i });
    sliderRef.slickGoTo(i);
  }

  goToNextSlide() {
    if (this.sliderRef) {
      this.sliderRef.slickNext()
    }
  }

  goToPrevSlide() {
    if (this.sliderRef) {
      this.sliderRef.slickPrev()
    }
  }

  handleSlideItemClick = (i) => {
    const { currentActive } = this.state;

    if (currentActive - 1 === i) {
      this.setState({ currentActive: i });
      this.goToPrevSlide();
    } else if (currentActive + 1 === i) {
      this.setState({ currentActive: i });
      this.goToNextSlide();
    }
  }

  render() {
    const { currentActive } = this.state;
    const { title, subtitle, images } = this.props;
    const debouncedItemClickHandler = debounce(this.handleSlideItemClick, 50);

    return (
      <div className='vr-show-photos vr-section' style={{ backgroundColor: '#1f1f1f' }}>
        <VrSectionBanner title={title} subtitle={subtitle} inverted />
        <div className="vr-show-photos__container">
          <div className="container-fluid">
            <div className="row center-xs justify">
              <div className="col-xs-12">
                <Slider {...this.sliderSettings} ref={slider => {this.sliderRef = slider}} className="vr-show-photos__slider">
                  {images.map(({ image }, i) =>
                    <div className="vr-show-photos__thumbs-item" key={shortid.generate()} onClick={() => debouncedItemClickHandler(i)}>
                      <img src={image.url} alt={image.title} />
                    </div>
                  )}
                </Slider>
              </div>
            </div>
            <div className="row center-xs justify">
              <div className="col-xs-12">
                <Slider {...this.thumbsSettings} ref={thumbs => { this.thumbsRef = thumbs}} className="vr-show-photos__thumbs">
                  {images.map(({ image }, i) =>
                    <div onClick={() => this.handleThumbClick(i)} className={`vr-show-photos__thumbs-item ${currentActive === i ? 'active' : ''}`} key={shortid.generate()}>
                      <img src={image.sizes.thumbnail} alt={image.title} />
                    </div>
                  )}
                </Slider>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
};
