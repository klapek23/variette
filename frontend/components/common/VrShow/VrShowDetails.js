import React from 'react';
import VrSectionBanner from '../VrSectionBanner';
import VrTabs from '../VrTabs';

export default function VrShowDetails({
  title,
  subtitle,
  tabs,
}) {
  return (
    <div
      className="vr-show-details vr-section"
      style={{ backgroundColor: '#fff' }}
    >
      <VrSectionBanner title={title} subtitle={subtitle} inverted dark />
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12 col-lg-10 col-lg-offset-1">
            <VrTabs tabs={tabs} />
          </div>
        </div>
      </div>
    </div>
  );
}
