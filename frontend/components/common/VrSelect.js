import shortid from 'shortid';
import React, { useState } from 'react';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import { motion } from 'framer-motion';
import OutsideClickHandler from 'react-outside-click-handler';

const itemAnimateProps = {
  initial: {
    opacity: 0,
  },
  animate: {
    opacity: 1,
  },
};

export default function VrSelect({
  items,
  currentItem,
  onChange,
  className = '',
}) {
  const [opened, setOpened] = useState(false);

  const handleItemClicked = (value) => {
    setOpened(false);
    onChange(value);
  };

  const handleClickedOutside = () => {
    setOpened(false);
  };

  return (
    <div className={`vr-select ${className}`}>
      <OutsideClickHandler onOutsideClick={handleClickedOutside}>
        <button
          type="button"
          className="vr-select__current"
          onClick={() => setOpened(!opened)}
        >
          <span className="vr-select__current-value">{currentItem.label}</span>
          {opened ? (
            <ArrowDropUpIcon className="vr-select__current-icon" />
          ) : (
            <ArrowDropDownIcon className="vr-select__current-icon" />
          )}
        </button>
        {opened ? (
          <motion.ul className={`vr-select__list`} {...itemAnimateProps}>
            {items.map((item) => (
              <li key={shortid.generate()}>
                <button type="button" onClick={() => handleItemClicked(item)}>
                  {item.label}
                </button>
              </li>
            ))}
          </motion.ul>
        ) : null}
      </OutsideClickHandler>
    </div>
  );
}
