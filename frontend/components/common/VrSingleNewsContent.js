import React from 'react';
import moment from 'moment';
import shortid from 'shortid';

import ArrowLeft from '../../static/arrow_left.svg';
import ArrowRight from '../../static/arrow_right.svg';

import VrLink from './VrLink';
import VrArrowButton from "./VrArrowButton";

export default function VrSingleNewsContent({
  date,
  content,
  categories,
  mainUrl,
  next,
  prev,
  labels,
}) {
  return (
    <div className="vr-single-news-content vr-section">
      <div className="vr-single-news-content__date">
        <span>{moment(date).format('LL')}</span>
      </div>
      <div className="vr-single-news-content__content">
        <article dangerouslySetInnerHTML={{ __html: content }} />
      </div>
      {categories && (
        <div className="vr-single-news-content__category">
          <p>
            {labels.category}:{' '}
            {categories.map(({ name, id }) => (
              <VrLink as={`${mainUrl}?category=${id}`} key={shortid.generate()}>
                {name}
              </VrLink>
            ))}
          </p>
        </div>
      )}
      <div className="vr-single-news-content__footer">
        {prev && (
          <VrLink
            as={`${mainUrl}/${prev.slug}`}
            href={`${mainUrl}/[slug]`}
            title={prev.title}
            className="vr-single-news-content__footer-link vr-single-news-content__footer-link--prev"
          >
              <div className="vr-button vr-arrow-button">
                <ArrowLeft />
              </div>
            <span>{labels.prev}</span>
          </VrLink>
        )}
        <VrLink
          as={mainUrl}
          href={mainUrl}
          className="vr-single-news-content__footer-link vr-single-news-content__footer-link--main"
        >
          <span>{labels.back}</span>
        </VrLink>
        {next && (
          <VrLink
            as={`${mainUrl}/${next.slug}`}
            href={`${mainUrl}/[slug]`}
            title={next.title}
            className="vr-single-news-content__footer-link vr-single-news-content__footer-link--next"
          >
            <span>{labels.next}</span>
              <div className="vr-button vr-arrow-button">
                  <ArrowRight />
              </div>
          </VrLink>
        )}
      </div>
    </div>
  );
}
