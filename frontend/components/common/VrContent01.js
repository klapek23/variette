import React from 'react';
import VrSectionBanner from './VrSectionBanner';

const VrContent01 = ({ children, title, subtitle, dark = false, bg }) => (
  <div className={`vr-content-01 vr-section ${dark && 'vr-content-01--dark'}`} style={{ backgroundImage: `url('${bg ? bg.url : ''}')` }}>
    <div className="vr-content-01__cover" />
    <div className="vr-content-01__content">
      <VrSectionBanner
        title={title}
        subtitle={subtitle}
        inverted
        dark={!dark}
      />
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-lg-7 col-lg-offset-1">
            {children}
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default VrContent01;
