import React from 'react';

const VrBottomAddressBar = ({ children }) => (
  <div className="vr-bottom-address-bar vr-section">
    <div className="container-fluid">
      <div className="row">
        <div className="col-xs-12">
          <div className="vr-bottom-address-bar__content">
            {children}
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default VrBottomAddressBar;
