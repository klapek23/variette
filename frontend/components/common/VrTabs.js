import React, { useRef, useState } from 'react';
import shortid from 'shortid';
import { AnimatePresence, motion } from 'framer-motion';
import VrButton from './VrButton';

const itemAnimateProps = {
  initial: {
    y: 100,
    opacity: 0,
  },
  animate: {
    y: 0,
    opacity: 1,
  },
  exit: {
    y: 100,
    opacity: 0,
  },
};

export default function VrTabs({ tabs }) {
  const ref = useRef(null);
  const [currentTab, setCurrentTab] = useState(0);

  const onClickHandler = (i) => {
    setCurrentTab(i);
  };

  return (
    <div className="vr-tabs">
      <nav className="vr-tabs__nav">
        <ul>
          {tabs.map(({ label }, i) => (
            <li key={shortid.generate()}>
              <VrButton
                className={`${currentTab === i && 'vr-button--grey--active'}`}
                color="grey"
                smallWithBigText
                onClick={() => onClickHandler(i)}
              >
                {label}
              </VrButton>
            </li>
          ))}
        </ul>
      </nav>

      <motion.div
        className="vr-tabs__container"
        animate={{
          height: 'auto',
        }}
      >
        {tabs.map(({ details }, i) =>
          currentTab === i ? (
            <AnimatePresence key={shortid.generate()}>
              <motion.div
                ref={ref}
                className="vr-tabs__box"
                {...itemAnimateProps}
              >{React.cloneElement(details)}
              </motion.div>
            </AnimatePresence>
          ) : null,
        )}
      </motion.div>
    </div>
  );
}
