import React from 'react';
import classNames from 'classnames';

const VrSectionBanner = ({
  title,
  subtitle,
  info,
  className = '',
  dark,
  children,
  inverted,
  main = false,
  nouppercase = false,
}) => {
  const classnames = classNames('vr-section-banner', {
    'vr-section-banner--dark': dark,
    'vr-section-banner--inverted': inverted,
    'vr-section-banner--has-info': info,
    'vr-section-banner--nouppercase': nouppercase,
  });
  return (
    <div className={`${classnames} ${className}`}>
      <div className="container-fluid">
        <div className="row center-xs">
          <div className="col-xs-12">
            <div className="vr-section-banner__caption">
              {inverted ? (
                <>
                  {info && <div>{info}</div>}
                  {main ? (
                    <h1 dangerouslySetInnerHTML={{ __html: title }} />
                  ) : (
                    <h2 dangerouslySetInnerHTML={{ __html: title }} />
                  )}
                  <div>{subtitle}</div>
                </>
              ) : (
                <>
                  <div>{subtitle}</div>
                  {main ? (
                    <h1 dangerouslySetInnerHTML={{ __html: title }} />
                  ) : (
                    <h2 dangerouslySetInnerHTML={{ __html: title }} />
                  )}
                  {info && <div>{info}</div>}
                </>
              )}
            </div>
          </div>
        </div>
        {children ? (
          <div className="row center-xs">
            <div className="col-xs-12">{children}</div>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default VrSectionBanner;
