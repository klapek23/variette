import React from 'react';
import AwesomeSlider from 'react-awesome-slider';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
import shortid from 'shortid';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import ArrowLeft from '../../static/arrow_left.svg';
import ArrowRight from '../../static/arrow_right.svg';
import VrLink from './VrLink';
import debounce from '../../utils/debounce';
import SpinnerSvg from '../../static/spinner.svg';

const AutoplaySlider = withAutoplay(AwesomeSlider);

const StartupScreen = () => {
  return <div className='vr-slider__startup-screen'>
    <div className="loader">
      <SpinnerSvg />
    </div>
  </div>
};

const VrSliderItem = ({
  title,
  subtitle,
  image,
  button_1: button1,
  button_2: button2,
}) => (
  <div
    className="vr-slider__child"
    style={{ backgroundImage: `url('${image.url}')` }}
    key={shortid.generate()}
  >
    <article className="vr-slider__caption">
      <h2 dangerouslySetInnerHTML={{ __html: title }} />
      <p dangerouslySetInnerHTML={{ __html: subtitle }} />
      <div className="vr-slider__caption-buttons">
        {button1 ? (
          <VrLink as={button1.url} className="vr-button vr-button--big">
            {button1.title}
          </VrLink>
        ) : null}
        {button2 ? (
          <VrLink as={button2.url} className="vr-button vr-button--big">
            {button2.title}
          </VrLink>
        ) : null}
      </div>
    </article>
  </div>
);


export default class VrSlider extends React.Component {
  constructor(props) {
    super(props);
    this.debouncedMapWindowSizeToSlides = debounce(this.mapWindowSizeToSlides, 200);
    this.debouncedUpdateWindowDimensions = debounce(this.updateWindowDimensions, 200);
    this.state = { height: 0, width: 0, slides: props.slides.slides_md, isPlaying: true };
  }

  componentDidMount() {
    import('o9n').then(({ getOrientation }) => {
      this.getOrientation = getOrientation;

      setTimeout(() => {
        this.updateWindowDimensions();
        this.setState({
          slides: this.mapWindowSizeToSlides(),
        });
      }, 0);
      window.addEventListener('resize', this.debouncedUpdateWindowDimensions);
      window.addEventListener('resize', this.debouncedMapWindowSizeToSlides);
    });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.debouncedUpdateWindowDimensions);
    window.removeEventListener('resize', this.debouncedMapWindowSizeToSlides);
  }

  updateWindowDimensions = () => {
    this.setState({ height: window.innerHeight, width: window.innerWidth });
  };


  mapWindowSizeToSlides = () => {
    const { slides } = this.props;

    const screenOrientation = this.getOrientation ? this.getOrientation().type : undefined;
    const sizes = [
      {
        max: 575,
        slides: 'slides_xs',
      },
      {
        min: 576,
        max: 767,
        slides: 'slides_sm',
      },
      {
        min: 768,
        max: 991,
        slides: 'slides_md',
      },
      {
        min: 992,
        max: 1199,
        slides: 'slides_lg',
      },
      {
        min: 1200,
        slides: 'slides_xl',
      },
    ];
    const { width } = this.state;
    let size = sizes.find(({ min, max }) => {
      if (min && !max) {
        return width >= min;
      }
      if (max && !min) {
        return width <= max;
      }

      return width >= min && width <= max;
    }).slides;

    const { isMobile } = this.props;

    if (isMobile && (screenOrientation === 'landscape-primary' || screenOrientation === 'landscape-secondary')) {
      size = 'slides_mobile_landscape';
    }

    const fallback = (items, notFoundSize) => {
      const itemsArray = Object.entries(items);
      const notFoundIndex = itemsArray.findIndex(([label]) => label === notFoundSize);
      const filteredSlides = itemsArray.filter(([label, value]) => !!value).map(([label, value]) => value);

      if (filteredSlides[notFoundIndex]) {
        return filteredSlides[notFoundIndex];
      }

      return filteredSlides[notFoundIndex - 1]
        ? filteredSlides[notFoundIndex - 1]
        : filteredSlides[0]
    };

    return slides[size] || fallback(slides, size);
  };

  setIsPlaying(value) {
    this.setState({
      isPlaying: value
    });
  }

  render() {
    const { height, width, slides, isPlaying } = this.state;
    const { play, isMobile } = this.props;
    const topElementsHeight = width < 575 ? 100 : 120;
    const calculateHeight = () => height - topElementsHeight > 0 ? `${height - topElementsHeight}px` : '0px';

    return (
      <div
        className={`vr-slider ${isMobile ? 'vr-slider--mobile' : ''}`}
        style={{ height: calculateHeight() }}
      >
        <button className="vr-button play-pause-button" onClick={() => this.setIsPlaying(!isPlaying)}>
          {isPlaying ? <PauseIcon /> : <PlayArrowIcon />}
        </button>

        <AutoplaySlider
          play={play && isPlaying}
          startupScreen={<StartupScreen />}
          cancelOnInteraction={false}
          bullets={false}
          organicArrows={false}
          buttonContentLeft={<ArrowLeft />}
          buttonContentRight={<ArrowRight />}
          fillParent
          name="vr-slider"
          interval={4000}
        >
          {slides.map((item) => (
            <div className="vr-slider__item" key={shortid.generate()}>
              <VrSliderItem {...item} />
            </div>
          ))}
        </AutoplaySlider>
      </div>
    );
  }
}
