import React, { useEffect, useState } from 'react';
import Vivus from 'vivus';
import useIsInViewport from 'use-is-in-viewport';
import VrContentBoxes from './VrContentBoxes';
import VrLink from './VrLink';
import VrButton from './VrButton';

import NewsSvg from '../../static/notes.svg';

export default function VrNews({
  title,
  subtitle,
  items,
  onButtonClick,
  white,
  disabled,
  label,
  animated,
  index = false
}) {
  const [vivus, setVivus] = useState(null);
  const [isInViewport, targetRef] = useIsInViewport({ threshold: 30 });

  useEffect(() => {
    if (animated) {
      setVivus(
        new Vivus(
          'news-svg',
          {
            type: 'delayed',
            duration: 200,
            start: 'inViewport',
          },
          () => {
            const animatedElement = document.getElementById('news-svg');
            if (animatedElement) {
              animatedElement.classList.add('ready');
            }
          },
        ),
      );
    }
  }, []);

  if (animated && isInViewport) {
    vivus.play();
  }

  return (
    <div
      className={`vr-news ${white ? 'vr-news--white' : ''} ${
        animated ? 'vr-news--animated' : ''
      } ${index ? 'vr-news--index' : ''}`}
    >
      {animated && (
        <div className="abc" ref={targetRef}>
          <NewsSvg id="news-svg" />
        </div>
      )}

      <VrContentBoxes
        title={title}
        subtitle={subtitle}
        items={items}
        invertedHeader
        white={white}
        main={index}
        className="vr-section--no-bottom-padding"
      />

      <div className="vr-news-footer">
        {onButtonClick ? (
          <VrButton
            disabled={disabled}
            onClick={onButtonClick}
            color={white ? 'red' : 'black'}
          >
            {label}
          </VrButton>
        ) : (
          <VrLink as="/aktualnosci" href="/aktualnosci" className={`vr-button vr-button--${white ? 'red' : 'black'}`}>
            {label}
          </VrLink>
        )}
      </div>
    </div>
  );
}
