import React from 'react';

const VrIframe = ({ children }) => (
  <div className="vr-iframe vr-section">
    {children}
  </div>
);

export default VrIframe;
