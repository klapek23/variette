import React from 'react';
import VrButton from './VrButton';

export default function VrLoadMoreItems({ onClick, disabled, label }) {
  return (
    <div className="vr-load-more-items vr-section">
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12 center-xs">
            {disabled}
            <VrButton disabled={disabled} onClick={onClick}>
              {label}
            </VrButton>
          </div>
        </div>
      </div>
    </div>
  );
}
