import React from 'react';

export default function VrFullWidthSection({children}) {
  return <div className="vr-full-width-section">{children}</div>;
}
