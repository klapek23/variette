import React from 'react';
import Link from 'next/link';

import {ConfigConsumer} from "../ConfigContext";

// href = path to page file
// as = url address in browser bar

const VrLink = ({ children, as, href, title, className = '', config, target, tabIndex = 0, prefetch = true }) => {
  const isRelative = (url) => url.startsWith('/');
  const isInternal = (url) => url.startsWith(config.wpUrl);

  const removeWpPrefix = (url) => url.replace(config.wpUrl, '');
  const removeTrailingSlash = (url) => url.replace(/\/$/, '');

  if (typeof href === 'string') {
    const url = removeTrailingSlash(as);
    const fullUrl = config.locale === 'en_US' ? `/en${url}` : url;

    if (isRelative(as)) {
      return (
        <Link href={fullUrl} prefetch={prefetch}>
          <a className={`vr-link ${className}`} title={title} target={target || undefined} tabIndex={tabIndex}>
            {children}
          </a>
        </Link>
      );
    } else if (isInternal(href)) {
      return (
        <Link href={removeWpPrefix(fullUrl)} prefetch={prefetch}>
          <a className={`vr-link ${className}`} title={title} target={target || undefined} tabIndex={tabIndex}>
            {children}
          </a>
        </Link>
      );
    }
  } else if (typeof href === 'object' && href.pathname && href.query) {
    const fullUrl = config.locale === 'en_US' ? `/en${as}` : as;
    return (
      <Link href={fullUrl} prefetch={prefetch}>
        <a className={`vr-link ${className}`} title={title} target={target || undefined} tabIndex={tabIndex}>
          {children}
        </a>
      </Link>
    );
  }

  return (
    <a className={`vr-link ${className}`} href={as} title={title} target={target || undefined} tabIndex={tabIndex}>
      {children}
    </a>
  );
};

const VrLinkWithConfigContext = (props) => (
  <ConfigConsumer>
    {(config) => (<VrLink { ...props } config={config} />)}
  </ConfigConsumer>
);

export default VrLinkWithConfigContext;
