import React from 'react';
import classNames from 'classnames';

import VrSectionBanner from './VrSectionBanner';
import VrFullWidthSection from './VrFullWidthSection';

const VrContent03 = ({
  children,
  title,
  subtitle,
  image,
  bgColor = '#fff',
  inverted = false,
  equalColumns = false,
  white = false,
}) => {
  const classnames = classNames('vr-content-03', 'vr-section', {
    'vr-content-03--inverted': inverted,
    'vr-content-03--equal-columns': equalColumns,
    'vr-content-03--white': white,
  });

  return (
    <div className={classnames} style={{ backgroundColor: bgColor }}>
      <VrSectionBanner
        title={title}
        subtitle={subtitle}
        dark={!white}
        inverted
      />
      <VrFullWidthSection>
        <div className="container-fluid">
          <div className="row middle-xs">
            {inverted ? (
              <>
                <div
                  className={
                    !equalColumns ? `col-xs-12 col-lg-4` : `col-xs-12 col-lg-6`
                  }
                >
                  <div className="vr-content-03__image">
                    <img src={image.url} alt={image.title} />
                  </div>
                </div>
                <div
                  className={
                    !equalColumns ? `col-xs-12 col-lg-8` : 'col-xs-12 col-lg-6'
                  }
                >
                  {children}
                </div>
              </>
            ) : (
              <>
                <div
                  className={
                    !equalColumns ? `col-xs-12 col-lg-8` : 'col-xs-12 col-lg-6'
                  }
                >
                  {children}
                </div>
                <div
                  className={
                    !equalColumns
                      ? `col-xs-12 col-lg-4 first-xs last-lg`
                      : `col-xs-12 col-lg-6 first-xs last-lg`
                  }
                >
                  <div className="vr-content-03__image">
                    <img src={image.url} alt={image.title} />
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </VrFullWidthSection>
    </div>
  );
};

export default VrContent03;
