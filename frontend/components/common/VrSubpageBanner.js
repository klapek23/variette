import React from 'react';
import VrSectionBanner from './VrSectionBanner';

export default function VrSubpageBanner({
  title,
  subtitle,
  image,
  dark = false,
  nouppercase = false,
}) {
  const style = { backgroundImage: image ? `url('${image.url}')` : '' };
  return (
    <div
      className={`vr-subpage-banner ${dark ? 'vr-subpage-banner--dark' : ''}`}
      style={style.backgroundImage ? style : {}}
    >
      <VrSectionBanner
        title={title}
        subtitle={subtitle}
        main
        dark={!dark}
        nouppercase={nouppercase}
      />
    </div>
  );
}
