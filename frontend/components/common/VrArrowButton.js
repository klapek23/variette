import React from 'react';
import classNames from 'classnames';
import ArrowLeft from '../../static/arrow_left.svg';
import ArrowRight from '../../static/arrow_right.svg';

const VrArrowButton = (
  {
    disabled = false,
    className = '',
    direction = 'right',
    onClick = () => null
  }
) => {
  const classnames = classNames('vr-button vr-arrow-button', className);
  return (
    <button
      type="button"
      disabled={disabled}
      className={classnames}
      onClick={onClick}
      aria-label={direction === 'right' ? 'next' : 'prev'}
    >
      {direction === 'right' ? <ArrowRight /> : <ArrowLeft />}
    </button>
  );
}

export default VrArrowButton;
