import React from 'react';
import shortid from 'shortid';

export default function VrFiles({ items }) {

  const formatBytes = (bytes, decimals = 2) => {
    if (bytes === 0) return '0 Bytes';
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return `${parseFloat((bytes / k ** i).toFixed(dm))} ${sizes[i]}`;
  }

  const extension = ({filename}) => {
    const arr = filename.split('.');
    return arr ? arr[arr.length - 1] : '';
  };
  return (
    <div className="vr-files vr-section">
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12">
            <ul className="vr-files__list">
              {items.map(({ file, label, link }) => (
                <li className="vr-files__list-item" key={shortid.generate()}>
                  {file && file.url && <a
                    href={file.url}
                    title={label}
                    rel="noreferrer"
                    target="_blank"
                  >
                    <div>
                      {label} <span className="vr-files__list-item-details">({extension(file)}, {formatBytes(file.filesize)})</span>
                    </div>
                    <em>{link.title}</em>
                  </a>}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
