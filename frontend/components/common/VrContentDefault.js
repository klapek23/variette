import React from 'react';
import classNames from 'classnames';

import VrFullWidthSection from './VrFullWidthSection';

export default function VrContentDefault({content, margin}) {
  const classnames = classNames('vr-section', 'vr-content-default', {
    'vr-content-default--margin': margin,
  });

  return (
    <div className={classnames}>
      <VrFullWidthSection>
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-12 col-lg-11 col-lg-offset-1">
              <article dangerouslySetInnerHTML={{__html: content}}/>
            </div>
          </div>
        </div>
      </VrFullWidthSection>
    </div>
  );
}
