import React from 'react';
import ReactPlayer from 'react-player';
import PlayCircleFilledWhiteOutlinedIcon from '@material-ui/icons/PlayCircleFilledWhiteOutlined';
import useLocalStorageState from "../../utils/hooks/useLocalStorageState";

const PlayButton = () => <PlayCircleFilledWhiteOutlinedIcon />;

const VrPlayer = ({ url, poster = '/static/poster.jpg' }) => {
  const [display] = useLocalStorageState(false, 'display_youtube');
  return display && (
    <div className="vr-player">
      <ReactPlayer
        className="vr-player__video"
        url={url}
        config={{ file: { attributes: { poster } } }}
        width="100%"
        height="100%"
        light={poster}
        controls
        playIcon={<PlayButton />}
      />
    </div>
  )
};

export default VrPlayer;
