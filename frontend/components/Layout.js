import React from 'react';
import Header from './Header';
import VrHeader from "./VrHeader";

const Layout = props => {
  const { children, menu, socials, pathname } = props;
  return (
    <>
      <Header />
      <main>
        <VrHeader socials={socials} menu={menu} pathname={pathname} />
        <div className="vr-main-content">
          {children}
        </div>
      </main>
    </>
  );
};

export default Layout;
