import * as React from 'react';

const { Provider, Consumer } = React.createContext(null);

export { Provider as ThemeOptionsProvider, Consumer as ThemeOptionsConsumer };