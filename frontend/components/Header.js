import React from 'react';
import Head from 'next/head';

import flexboxgrid from 'flexboxgrid2/flexboxgrid2.css';
import reactawesomesliderstyles from 'react-awesome-slider/dist/styles.css';
import reactresponsivecarouselstyles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import hamburgericon from '../src/styles/external/hamburgers.min.css';
import stylesheet from '../src/styles/style.scss';

const Header = () => (
  <Head>
    <link rel="shortcut icon" href="/static/favicon.ico" />
    <link
      rel="stylesheet"
      type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
    />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,400&family=Roboto:ital,wght@0,300;0,500&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
    />

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2oAGsMmvqfmt2FBOOtZTpcDwVJqicK4E&map_ids=6a9e8875bc5e0420" />

    <style
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{ __html: flexboxgrid }}
    />
    <style
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{ __html: hamburgericon }}
    />
    <style
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{ __html: reactawesomesliderstyles }}
    />
    <style
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{ __html: reactresponsivecarouselstyles }}
    />
    <style
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{ __html: stylesheet }}
    />
  </Head>
);

export default Header;
