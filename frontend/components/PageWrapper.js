import Head from 'next/head';
import React from 'react';
import WPAPI from 'wpapi';
import moment from 'moment';
import DOMparser from 'dom-parser';
import Config from '../config';
import createBreadcrumbs from '../utils/breadcrumbs';
import CookiesNotice from './common/CookiesNotice';
import { ConfigProvider } from './ConfigContext';
import { ThemeOptionsProvider } from './ThemeOptionsContext';
import Footer from "./Footer";
// eslint-disable-next-line no-unused-expressions
import('moment/locale/pl');
// eslint-disable-next-line no-unused-expressions
import('moment/locale/en-gb');

const stripHtml = (text) => (text ? text.replace(/<[^>]*>?/gm, '') : '');

const PageWrapper = (lang) => (Comp) =>
  class extends React.Component {
    state = {
      displayGA: false,
      showFooter: true,
    };

    static async getInitialProps(args) {
      const config = Config[lang];

      const wp = new WPAPI({ endpoint: config.apiUrl });
      // This route is copied from the plugin: wordpress/wp-content/plugins/wp-rest-api-v2-menus/wp-rest-api-v2-menus.php
      wp.menus = wp.registerRoute('menus/v1', '/menus/(?P<id>[a-zA-Z(-]+)');

      wp.cookiesSettings = wp.registerRoute('cookies-by-klapek23/v1', '/settings');
      wp.repertoire = wp.registerRoute('wp/v2', 'repertoire/(?P<id>)');
      wp.direction = wp.registerRoute('wp/v2', 'direction/(?P<id>)');
      wp.themeOptions = wp.registerRoute('custom', '/theme-options');
      wp.years = wp.registerRoute('custom', 'posts-years');
      wp.postscategories = wp.registerRoute('custom', 'posts-categories');
      
      moment.locale(lang);

      const [
        headerMenu,
        footerMenu,
        themeOptions,
        cookiesSettings,
        childProps,
      ] = await Promise.all([
        wp.menus().id('header-menu'),
        wp.menus().id('footer-menu'),
        wp.themeOptions(),
        wp.cookiesSettings(),

        Comp.getInitialProps ? Comp.getInitialProps({ ...args, wp, config }) : {},
      ]);

      const breadcrumbs = createBreadcrumbs(args.pathname, args.asPath, config.locale);

      const { page } = childProps;

      const ogTags = page ? {
        appId: themeOptions.fbAppId || '',
        title: stripHtml(page.title.rendered),
        description: stripHtml(
          page.excerpt
            ? page.excerpt.rendered
            : `${page.content.rendered.substring(0, 150)}...`,
        ),
        type: page.type === 'post' ? 'article' : 'website',
        image:
          page._embedded && page._embedded['wp:featuredmedia']
            ? page._embedded['wp:featuredmedia'][0]
            : '',
        locale: config.locale,
        url: `${config.publicFrontendUrl}${args.asPath}`,
      } : {};

      return {
        headerMenu,
        themeOptions,
        footerMenu,
        breadcrumbs,
        cookiesSettings,
        ogTags,
        config,
        wp,
        pathname: args.pathname,
        ...childProps,
      };
    }

    componentDidMount() {
      const displayGA = window.localStorage.getItem('display_ga');
      const { page } = this.props;
      this.setState({ displayGA, showFooter: !(page && page.slug === 'home-page') })
    }

    setFooterDisplay = (value) => {
      this.setState({ showFooter: value });
    }

    render() {
      const { cookiesSettings, ogTags, config, page, themeOptions, footerMenu } = this.props;
      const parser = new DOMparser();
      const seo = page && page.acf && page.acf.seo ? page.acf.seo : {
        description: '',
        keywords: '',
      };
      const pageTitle = parser.parseFromString(`${config.title} - ${page && page.title.rendered ? page.title.rendered.replace(/<[^>]*>?/gm, '').replace('&#038;', '&') : ''}`);
      const { displayGA, showFooter } = this.state;

      const props = { ...this.props, setFooterDisplay: this.setFooterDisplay };

      moment.locale(lang);

      return (
        <div>
          <Head>
            <meta charSet="utf-8" />
            <meta
              name="viewport"
              content="minimum-scale=1, initial-scale=1, width=device-width"
            />
            <title>{seo.title || pageTitle.rawHTML}</title>
            <meta name="description" content={seo.description} />
            <meta name="keywords" content={seo.keywords} />

            <meta property="fb:app_id" content={ogTags.appId} />
            <meta property="og:title" content={ogTags.title} />
            <meta property="og:description" content={ogTags.description} />
            {ogTags.image ? (
              <meta property="og:image" content={ogTags.image.url} />
            ) : null}
            <meta property="og:url" content={ogTags.url} />
            <meta property="og:type" content={ogTags.type} />
            <meta property="og:locale" content={ogTags.locale} />

            {displayGA && <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113775481-1" />}
            {displayGA && <script dangerouslySetInnerHTML={{
              __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', 'UA-113775481-1');
            `,
            }} />}
          </Head>
          <ConfigProvider value={config}>
            <ThemeOptionsProvider value={themeOptions}>
              <Comp {...props} />
              {showFooter && <Footer menu={footerMenu} footerThemeOptions={themeOptions} socials={themeOptions.socials} />}
            </ThemeOptionsProvider>
          </ConfigProvider>
          <div id="vr-portal" />
          {cookiesSettings && <CookiesNotice {...cookiesSettings} labels={{ close: lang === 'pl' ? 'Zamknij' : 'Close' }} />}
        </div>
      );
    }
  };

export default PageWrapper;
