/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import Router from 'next/router';
import VrMobileMenu from './common/VrMenu/VrMobileMenu';
import VrDesktopMenu from "./common/VrMenu/VrDesktopMenu";

class Menu extends Component {
  state = {
    scrolled: false,
    isMenuActive: false,
    windowWidth: 0
  };

  constructor() {
    super();
    this.menu = React.createRef();
  }

  componentDidMount() {
    this.resizeHandler();
    this.scrollHandler();

    window.addEventListener('resize', this.resizeHandler);
    window.addEventListener('scroll', this.scrollHandler);

    Router.events.on('routeChangeComplete', this.closeMobileMenu);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollHandler);
    window.removeEventListener('resize', this.resizeHandler);
    Router.events.off('routeChangeComplete', this.closeMobileMenu);
  }

  getCurrentTopBarHeight = (windowWidth) => {
    if (windowWidth > 991) {
      return 76;
    }
    if (windowWidth > 575) {
      return 60;
    }
    return 50;
  }

  resizeHandler = () => {
    this.setState({ windowWidth: window.innerWidth });
  }

  scrollHandler = () => {
    const { windowWidth } = this.state;
    const currentTopBarHeight = this.getCurrentTopBarHeight(windowWidth);
    this.setState({
      scrolled: window.scrollY > currentTopBarHeight,
    });
  };

  blockPageScroll = () => {
    const body = document.getElementsByTagName('body')[0];
    body.style.overflowY = 'hidden';
  };

  allowPageScroll = () => {
    const body = document.getElementsByTagName('body')[0];
    body.style.overflowY = 'auto';
  };

  openMobileMenu = () => {
    this.blockPageScroll();
    this.setState({ isMenuActive: true });
  };

  closeMobileMenu = () => {
    this.allowPageScroll();
    this.setState({ isMenuActive: false });
  };

  toggleMobileMenu = () => {
    const { isMenuActive } = this.state;
    if (!isMenuActive) {
      this.openMobileMenu();
    } else {
      this.closeMobileMenu();
    }
    this.setState({ isMenuActive: !isMenuActive });
  };

  render() {
    const { pathname, menu } = this.props;
    const { scrolled, isMenuActive } = this.state;
    return (
      <>
        <VrMobileMenu
          pathname={pathname}
          menu={menu}
          isMenuActive={isMenuActive}
        />
        <div
          className={`header-menu ${scrolled ? 'header-menu--scrolled' : ''}`}
          ref={this.menu}
        >
          <VrDesktopMenu pathname={pathname} menu={menu} />
          <button
            className={`hamburger hamburger--collapse ${
              isMenuActive ? 'is-active' : ''
            }`}
            type="button"
            onClick={this.toggleMobileMenu}
          >
            <span className="hamburger-box">
              <span className="hamburger-inner">menu button</span>
            </span>
          </button>
        </div>
      </>
    );
  }
}

export default Menu;
