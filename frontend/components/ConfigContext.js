import * as React from 'react';

const Context = React.createContext(null);

const { Consumer, Provider } = Context;

export { Provider as ConfigProvider, Consumer as ConfigConsumer };

export default Context;