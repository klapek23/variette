import React, {useContext} from 'react';
import Link from "next/link";
import Menu from "./Menu";
import VrTopBar from "./common/VrTopBar";
import ConfigContext from "./ConfigContext";

export default function VrHeader({socials, menu, pathname}) {
    const { locale } = useContext(ConfigContext);
    const homeUrl = locale === 'pl_PL' ? '/' : '/en';

  return (
    <div className="vr-header">
      <div className="vr-header__logo">
        <Link as={homeUrl} href={homeUrl}>
          <img src="/static/logo.png" alt="Variete logo" />
        </Link>
      </div>

      <VrTopBar socials={socials} />
      <Menu menu={menu} pathname={pathname} />
    </div>
  )
}
