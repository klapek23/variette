import React, { useEffect, useState } from 'react';
import useIsInViewport from 'use-is-in-viewport';
import Vivus from 'vivus';
import VrLink from './common/VrLink';
import VrButton from './common/VrButton';
import slugify from '../utils/slugify';
import FooterSvg from '../static/footer.svg';

const getSlug = (url) => {
  const parts = url.split('/');
  return parts.length > 2 ? parts[parts.length - 2] : '';
};

const Footer = ({ footerThemeOptions, socials, menu }) => {
  const renderMenuItems = (items) => {
    return items.map((item) => {
      const slug = getSlug(item.url);
      const type = item.object;
      let href, as;
      switch (type) {
        case 'page':
          href = slug;
          as = `/${slug}`;
          break;
        default:
          as = item.url;
          break;
      }
      return item.classes.includes('special') ? (
        <li key={`footer_${slugify(item.title)}`}>
          <VrLink as={as} href={href} key={item.ID} className="special">
            <VrButton color="black" small noBorder>
              {item.title}
            </VrButton>
          </VrLink>
        </li>
      ) : (
        <li key={`footer_${slugify(item.title)}`}>
          <VrLink as={as} href={href} key={item.ID}>
            {item.title}
          </VrLink>
        </li>
      );
    });
  };

  const [vivus, setVivus] = useState(null);
  const [isInViewport, targetRef] = useIsInViewport({ threshold: 30 });

  useEffect(() => {
    setVivus(
      new Vivus(
        'footer-svg',
        {
          duration: 300,
          start: 'inViewport',
        },
        () => {
          const animatedElement = document.getElementById('footer-svg');
          if (animatedElement) {
            animatedElement.classList.add('ready');
          }
        },
      ),
    );
  }, []);

  if (isInViewport) {
    vivus.play();
  }

  return (
    <>
      <div className="vr-footer">
        <div className="vr-footer__svg" ref={targetRef}>
          <FooterSvg id="footer-svg" />
        </div>

        <div className="vr-footer__content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-xs-12 col-md-6 col-xl-3">
                <div className="vr-footer__logo">
                  <VrLink as="/" tabIndex={-1}>
                    <img
                      src="/static/logo-alternative.png"
                      alt="Variete logo"
                    />
                  </VrLink>
                </div>
              </div>
              <div className="col-xs-12 col-md-6 col-xl-3">
                <article
                  dangerouslySetInnerHTML={{
                    __html: footerThemeOptions.footerContact,
                  }}
                />
              </div>
              <div className="col-xs-12 col-md-6 col-xl-3">
                <div className="row">
                  <div className="col-xs-12 col-sm-6 col-md-12">
                    <h4>Social Media</h4>
                    <ul>
                      {socials && socials.facebookUrl ? (
                        <li>
                          <VrLink as={socials.facebookUrl} title="Facebook">
                            Facebook
                          </VrLink>
                        </li>
                      ) : null}
                      {socials && socials.instagramUrl ? (
                        <li>
                          <VrLink as={socials.instagramUrl} title="Instagram">
                            Instagram
                          </VrLink>
                        </li>
                      ) : null}
                      {socials && socials.twitterUrl ? (
                        <li>
                          <VrLink as={socials.twitterUrl} title="Twitter">
                            Twitter
                          </VrLink>
                        </li>
                      ) : null}
                      {socials && socials.youtubeUrl ? (
                        <li>
                          <VrLink as={socials.youtubeUrl} title="Youtube">
                            Youtube
                          </VrLink>
                        </li>
                      ) : null}
                    </ul>
                  </div>
                  <div className="col-xs-12 col-sm-6 col-md-12">
                    <h4>Menu</h4>
                    <ul>{renderMenuItems(menu.items)}</ul>
                  </div>
                </div>
              </div>
              <div className="col-xs-12 col-md-6 col-xl-3">
                <article
                  dangerouslySetInnerHTML={{
                    __html: footerThemeOptions.footerTeatrVariete,
                  }}
                />
                <div className="vr-footer__logos">
                  <div className="vr-footer__logos-item">
                    <a
                      href="https://www.bip.krakow.pl/?bip_id=566"
                      title="BIP"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <img src="/static/logo_bip.png" alt="Logo BIP" />
                    </a>
                  </div>
                  <div className="vr-footer__logos-item">
                    <a
                      href="http://www.krakow.pl/"
                      title="Kraków"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <img src="/static/logo_krakow.png" alt="Logo Kraków" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="vr-footer__bottom">
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-12 col-md-3">
              <img
                src="/static/logo_ikmk.png"
                alt="Logo IKMK"
                className="logo-ikmk"
              />
            </div>
            <div className="col-xs-12 col-sm-5 col-md-4 col-xl-3 col-xl-offset-1">
              <p>{footerThemeOptions.footerInfo}</p>
            </div>
            <div className="col-xs-12 col-sm-4 col-md-3">
              <p>
                <VrLink
                  as={footerThemeOptions.privacyPolicy.url}
                  href={footerThemeOptions.privacyPolicy.url}
                >
                  {footerThemeOptions.privacyPolicy.label}
                </VrLink>
              </p>
            </div>
            <div className="col-xs-12 col-sm-3 col-md-2 last">
              <p>
                <a target="_blank" href="https://grandeidea.pl/" title="Grande Idea">
                  Grande Idea
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
