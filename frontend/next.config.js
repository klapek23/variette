const path = require('path');
const glob = require('glob');

module.exports = {
  env: {
    ROOT: __dirname,
  },
  async redirects() {
    return [
      {
        source: '/repertuar',
        destination: '/repertuar/sprawdz-repertuar',
        permanent: true,
      },
      {
        source: '/teatr',
        destination: '/teatr/bilety',
        permanent: true,
      },
      {
        source: '/repertoir',
        destination: '/repertoir/see-repertoir',
        permanent: true,
      },
      {
        source: '/theatre',
        destination: '/theatre/tickets',
        permanent: true,
      },
    ]
  },
  webpack: config => {
    config.module.rules.push(
      {
        test: /\.css$/,
        use: ['raw-loader', 'postcss-loader'],
      },
      {
        test: /\.s(a|c)ss$/,
        use: [
          'raw-loader',
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              includePaths: ['styles', 'node_modules']
                .map(d => path.join(__dirname, d))
                .map(g => glob.sync(g))
                .reduce((a, c) => a.concat(c), []),
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'react-svg-loader',
            options: {
              svgo: {
                plugins: [
                  { removeViewBox: false },
                  { mergePaths: false },
                  { cleanupIDs: false },
                  { removeUselessDefs: false }
                ],
              }
            }
          },
        ]
      },
    );
    return config;
  }
}
