export const isRequiredValid = value => !!value;

export const isEmail = value => /\S+@\S+\.\S+/g.test(value);

export const isPhone = value => /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/g.test(value);
