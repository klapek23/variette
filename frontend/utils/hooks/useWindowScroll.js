import { useState, useEffect } from 'react';
import debounce from '../debounce';

// Hook
function useWindowScroll() {
  // Initialize state with undefined width/height so server and client renders match
  // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
  const [windowPosition, setWindowPosition] = useState(0);

  useEffect(() => {
    // Handler to call on window resize
    function handleScroll() {
      // Set window width/height to state
      setWindowPosition(window.scrollY);
    }

    const debouncedHanlder = debounce(handleScroll, 100)

    // Add event listener
    window.addEventListener("scroll", debouncedHanlder);

    // Call handler right away so state gets updated with initial window size
    handleScroll();

    // Remove event listener on cleanup
    return () => window.removeEventListener("resize", debouncedHanlder);
  }, []); // Empty array ensures that effect is only run on mount

  return windowPosition;
}

export default useWindowScroll;
