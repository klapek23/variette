import React from "react";

export default function useLocalStorageState(defaultValue, key) {
  const [value, setValue] = React.useState(() => {
    const tmpValue = typeof window !== 'undefined' ? window.localStorage.getItem(key) : null;
    return tmpValue !== null
      ? JSON.parse(tmpValue)
      : defaultValue;
  });
  React.useEffect(() => {
    if (window) window.localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]);
  return [value, setValue];
}
