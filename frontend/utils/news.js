import moment from "moment";

export default function wpPosts2ContentBoxes(posts, lang = 'pl') {
  return posts.map(({ acf: postACF, image, excerpt, title, slug, date, _embedded, ...rest }) => ({
    ...rest,
    ...postACF,
    image: {
      url: _embedded['wp:featuredmedia'] && _embedded['wp:featuredmedia'][0] ? _embedded['wp:featuredmedia'][0].source_url : '',
      title: _embedded['wp:featuredmedia'] && _embedded['wp:featuredmedia'][0] ? _embedded['wp:featuredmedia'][0].title.rendered : '',
      alt: _embedded['wp:featuredmedia'] && _embedded['wp:featuredmedia'][0] ? _embedded['wp:featuredmedia'][0].alt : '',
    },
    title: postACF.customTitle || title.rendered || title,
    excerpt: excerpt.rendered,
    bgText: moment(date).format('DD/MM'),
    link: {
      as: `${lang !== 'pl' ? `/${lang}/` : ''}/aktualnosci/${slug}`,
      href: `${lang !== 'pl' ? `/${lang}` : ''}/aktualnosci/[slug]`,
      title: lang === 'pl' ? 'Czytaj więcej' : 'Read more',
    },
    slug
  }));
}
