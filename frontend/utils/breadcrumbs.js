function reduceBreadcrumbs(path) {
  const pieces = path.split('/').filter((item) => !!item);
  return pieces.reduce((previous, current) => {
    let newBreadcrumb;
    if (previous.length > 0) {
      newBreadcrumb = `${previous[previous.length - 1]}/${current}`;
    } else {
      newBreadcrumb = current;
    }
    return [ ...previous, newBreadcrumb ];
  }, []);
}

export default function createBreadcrumbs(path, asPath, locale) {
  const hrefArr = reduceBreadcrumbs(path);
  const asArr = reduceBreadcrumbs(asPath).map(item => `/${item}`);
  if (locale === 'en_US') {
    hrefArr.shift();
    asArr.shift();
  }

  return hrefArr.map((item, index) => ({ href: item, as: asArr[index] }));
}
