function slugify (str) {
  let newstring;
  newstring = str.replace(/^\s+|\s+$/g, ''); // trim
  newstring = newstring.toLowerCase();

  // remove accents, swap ñ for n, etc
  const from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  const to   = "aaaaeeeeiiiioooouuuunc------";
  for (let i=0, l=from.length ; i<l ; i++) {
    newstring = newstring.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  newstring = newstring.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return newstring;
}

export default slugify;
