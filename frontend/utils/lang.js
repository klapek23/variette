export const defaultLanguage = 'pl';

export const languages = [
  {
    href: '/',
    id: 'pl',
    label: 'PL',
  },
  {
    href: '/en',
    id: 'en',
    label: 'EN'
  },
];

export const isLang = (item) => languages.find(({ id }) => id === item);

export const getCurrentLanguage = (pathname) => {
  const firstPathSegment = pathname.split('/')[0];
  let lang = defaultLanguage;
  if (isLang(firstPathSegment)) {
    lang = firstPathSegment;
  }
  return languages.find(({ id }) => id === lang);
}
