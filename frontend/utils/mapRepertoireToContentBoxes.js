export default function mapRepertoireToContentBoxes(items, baseHref, lang = 'pl') {
  return items.map(({ slug, title, excerpt, image, ...rest }) => ({
    ...rest,
    slug,
    excerpt: excerpt.rendered,
    title: title.rendered,
    link: {
      as: `${baseHref}/${slug}`,
      href: `${baseHref}/[slug]`,
      title: lang === 'pl' ? 'Czytaj dalej' : 'Read more',
    },
    image: {
      url: image.source_url,
      alt: image.alt || image.alt_text || image.title,
      title: image.alt || image.alt_text || image.title
    },
  }));
}
