import React, { Component } from 'react';

import Layout from '../../components/Layout';
import PageWrapper from '../../components/PageWrapper';
import VrBreadcrumbs from '../../components/common/VrBreadcrumbs';
import VrTickets from '../../components/common/VrTickets';
import VrEqualBoxes from '../../components/common/VrEqualBoxes';
import VrLink from '../../components/common/VrLink';
import VrButton from '../../components/common/VrButton';
import VrContent01Animated from "../../components/common/VrContent01Animated";

import AudienceSvg from '../../static/audience.svg';

class Index extends Component {
  static async getInitialProps({wp}) {
    try {
      const [page] = await Promise.all([
        wp
          .pages()
          .slug('bilety')
          .embed()
          .then((data) => {
            return data[0];
          }),
      ]);

      return { page };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  render() {
    const {
      headerMenu,
      page,
      pathname,
      themeOptions,
      breadcrumbs,
    } = this.props;
    const { socials, ...otherThemeOptions } = themeOptions;
    const { acf } = page;
    const { section_01, section_02, section_03 } = acf;

    return (
      <>
        <Layout
          menu={headerMenu}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >
          <VrTickets
            title={section_01.title}
            subtitle={section_01.subtitle}
            titleInfo={<VrBreadcrumbs items={breadcrumbs} pageTitle={section_01.title} />}
            items={section_01.steps}
            button={section_01.button}
          />
          <VrEqualBoxes
            title={section_02.title}
            subtitle={section_02.subtitle}
            items={section_02.boxes}
          />
          <VrContent01Animated
            title={section_03.title}
            subtitle={section_03.subtitle}
            image={{ svg: AudienceSvg, id: 'audience-svg' }}
            dark
          >
            <article dangerouslySetInnerHTML={{ __html: section_03.content }} />
            {section_03.link ? (
              <VrLink as={section_03.link.url} title={section_03.link.title}>
                <VrButton>{section_03.link.title}</VrButton>
              </VrLink>
            ) : null}
          </VrContent01Animated>
        </Layout>
      </>
    );
  }
}

export default PageWrapper('pl')(Index);
