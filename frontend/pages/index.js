import React, { Component } from 'react';

import Layout from '../components/Layout';
import PageWrapper from '../components/PageWrapper';
import VrSlider from '../components/common/VrSlider';
import VrShows from '../components/common/VrShows';
import VrNewsletterSection from '../components/common/VrNewsletterSection';
import VrPlayer from '../components/common/VrPlayer';
import VrClients from '../components/common/VrClients';
import wpPosts2ContentBoxes from '../utils/news';
import { isEmail } from '../utils/validators';
import VrNews from '../components/common/VrNews';
import VrOpenTicket from '../components/common/VrOpenTicket';
import VrFrontPage from "../components/common/VrFrontPage";

class Index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sliderPlay: false,
      frontPageClosed: false
    }
  }

  static async getInitialProps({ wp, req }) {
    const ssr = !!req;

    try {
      const [page, posts] = await Promise.all([
        wp
          .pages()
          .slug('home-page')
          .embed()
          .then((data) => {
            return data[0];
          }),
        wp.posts().embed(),
      ]);

      const isMobileView = (req
        ? req.headers['user-agent']
        : navigator.userAgent
      ).match(
        /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
      );

      return { page, posts, isMobileView: !!isMobileView, ssr };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  handleFrontPageClosed = () => {
    const { setFooterDisplay } = this.props;
    this.setState({ frontPageClosed: true, sliderPlay: true });
    setFooterDisplay(true);
  };

  render() {
    const { sliderPlay, frontPageClosed } = this.state;
    const { headerMenu, page, themeOptions, posts, pathname, isMobileView, config, ssr } = this.props;
    const lastPosts = wpPosts2ContentBoxes(posts).filter((item, i) => i < 3);
    const { socials, ...otherThemeOptions } = themeOptions;
    const { acf } = page;
    const {
      main_slider: mainSlider,
      news_section: newsSection,
      shows,
      open_ticket: openTicket,
      newsletter_section: newsletterSection,
      videoplayer,
      clients,
    } = acf;

    const newsletterFormFields = [
      {
        defaultValue: '',
        type: 'text',
        name: 'name',
        fieldProps: {
          required: true,
          label: 'Twoje imię i nazwisko',
        },
        validationFn(value) {
          return !value ? 'To pole jest wymagane' : null;
        },
      },
      {
        defaultValue: '',
        type: 'text',
        subtype: 'email',
        name: 'email',
        fieldProps: {
          required: true,
          label: 'Twój e-mail',
        },
        validationFn(value) {
          if (!value) {
            return 'To pole jest wymagane';
          }
          if (!isEmail(value)) {
            return 'Podaj poprawny adres e-mail';
          }
          return null;
        },
      },
      {
        defaultValue: false,
        type: 'checkbox',
        name: 'rodo',
        fieldProps: {
          required: true,
          label:
            "* Wyrażam zgodę na przetwarzanie moich danych osobowych przez Krakowski Teatr VARIETE w celu\n" +
            "udzielenia odpowiedzi na pytanie zadane przez formularz kontaktowy za pośrednictwem kanałów\n" +
            "komunikacji wskazanych w formularzu.",
        },
        validationFn(value) {
          if (!value) {
            return 'To pole jest wymagane';
          }
          return null;
        },
      },
    ];

    return (
      <>
        {!isMobileView && <VrFrontPage title={config.title} subtitle="WYOBRAŹ SOBIE..." buttonLabel="WCHODZĘ" onClose={this.handleFrontPageClosed} />}

        <h1 className="hidden-title">Teatr Variete - Strona Krakowskiego Teatru VARIETE.</h1>

        <Layout
          menu={headerMenu}
          newsletterFormFields={newsletterFormFields}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >

          {isMobileView || frontPageClosed ? <div>
            <VrSlider slides={mainSlider} play={sliderPlay} isMobile={isMobileView} />

            <VrNews
                title={newsSection.title}
                subtitle={newsSection.subtitle}
                items={lastPosts}
                label="Więcej aktualności"
                animated
                index
            />

            <VrShows title={shows.title} subtitle={shows.subtitle} items={shows.items} />

            <VrOpenTicket
                title={openTicket.title}
                subtitle={openTicket.subtitle}
                content={openTicket.content}
                bg="/static/open_ticket_bg.jpg"
                link={openTicket.link}
            />

            <VrNewsletterSection
                title={newsletterSection.title}
                subtitle={newsletterSection.subtitle}
                fields={newsletterFormFields}
                submitLabel="Zapisz się"
                successMessage="Wiadomość wysłana pomyślnie"
            />

            {videoplayer && <VrPlayer url={videoplayer.video_url} />}

            {clients && <VrClients items={clients.items} />}
          </div> : null}
        </Layout>
      </>
    );
  }
}

export default PageWrapper('pl')(Index);
