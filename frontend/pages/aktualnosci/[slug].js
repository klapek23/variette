import React, { Component } from 'react';

import Layout from '../../components/Layout';
import PageWrapper from '../../components/PageWrapper';
import VrSubpageBanner from '../../components/common/VrSubpageBanner';
import VrSingleNewsContent from '../../components/common/VrSingleNewsContent';
import wpPosts2ContentBoxes from '../../utils/news';
import VrNewsList from '../../components/common/VrNewsList';
import VrNewsColumnList from '../../components/common/VrNewsColumnList';
import VrBreadcrumbs from '../../components/common/VrBreadcrumbs';

class Index extends Component {
  static async getInitialProps({ query, wp }) {
    const { slug } = query;
    try {
      const [page, news, years, categories] = await Promise.all([
        wp
          .posts()
          .slug(slug)
          .perPage(1)
          .embed()
          .then((data) => {
            return data[0];
          }),
        wp.posts().embed(),
        wp.years(),
        wp.postscategories().embed(),
      ]);

      const filteredNews = news.filter(({ id }) => id !== page.id);

      const preparedCategories = categories
        ? categories.map(({ name, term_id: id }) => {
            return {
              title: name,
              slug,
              url: `/aktualnosci?category=${id}`,
            };
          })
        : [];

      const preparedYears = years
        ? years.map((item) => ({
            title: item,
            slug: item,
            url: `/aktualnosci?year=${item}`,
          }))
        : [];

      return {
        page,
        news: filteredNews,
        years: preparedYears,
        categories: preparedCategories,
      };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  render() {
    const {
      headerMenu,
      page,
      news,
      years,
      pathname,
      categories,
      themeOptions,
      breadcrumbs,
    } = this.props;
    const { socials, ...otherThemeOptions } = themeOptions;
    const { acf } = page;
    const posts = wpPosts2ContentBoxes(news).filter((item, i) => i < 2);

    return (
      <>
        <Layout
          menu={headerMenu}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >
          <VrSubpageBanner
            title={acf.main_banner.title}
            subtitle={
              <VrBreadcrumbs
                items={breadcrumbs}
                pageTitle={acf.main_banner.title}
              />
            }
            image={acf.main_banner.image}
            dark
            nouppercase
          />

          <div className="vr-single-news">
            <div className="container-fluid">
              <div className="row">
                <div className="col-xs-12 col-lg-10 col-lg-offset-1">
                  <div className="row">
                    <div className="col-xs-12 col-lg-9 vr-single-news__left-column">
                      <VrSingleNewsContent
                        date={page.date}
                        categories={page._embedded['wp:term'][0]}
                        content={page.content.rendered}
                        mainUrl="/aktualnosci"
                        prev={page.previous}
                        next={page.next}
                        labels={{
                          category: 'Kategoria',
                          prev: 'Poprzedni wpis',
                          next: 'Kolejny wpis',
                          back: 'Wróć do aktualności',
                        }}
                      />
                    </div>
                    <div className="col-xs-12 col-lg-3 vr-single-news__right-column">
                      <VrNewsList title="Ostatnie wpisy" items={posts} />

                      <VrNewsColumnList title="Archiwum" items={years} />

                      <VrNewsColumnList
                        title="Kategorie"
                        items={categories}
                        paddingBottom
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Layout>
      </>
    );
  }
}

export default PageWrapper('pl')(Index);
