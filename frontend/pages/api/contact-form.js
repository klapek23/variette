import { isEmail, isPhone } from '../../utils/validators';
import { createTransporter, sendEmail } from '../../src/backend/smtp';
import messages from "../../src/backend/messages";

import Config from "../../config";

const config = Config.pl;
const root = process.env.ROOT;

const validateForm = (form) => {
  const errors = {};
  const formFields = Object.keys(form);
  formFields.forEach((field) => {
    const value = form[field];
    switch (field) {
      case 'name':
        if (!value) {
          errors[field] = messages.pl.required;
        }
        break;
      case 'email':
        if (!value) {
          errors[field] = messages.pl.required;
        }
        if (!isEmail(value)) {
          errors[field] = messages.pl.email;
        }
        break;
      case 'phone':
        if (!value) {
          errors[field] = messages.pl.required;
        }
        if (!isPhone(value)) {
          errors[field] = messages.pl.phone;
        }
        break;
      case 'message':
        if (!value) {
          errors[field] = messages.pl.required;
        }
        break;
      default:
        break;
    }
  });

  return Object.keys(errors).length > 0 ? errors : null;
};

export default async (req, res) => {
  const { email } = req.body;

  // Validate fields
  const errors = validateForm(req.body);

  if (!errors) {
    try {
      const transporter = createTransporter(config.smtp);
      sendEmail({
        sendFromEmail: config.smtp.smtpEmail,
        sendFromName: config.smtp.smtpName,
        sendToEmail: config.smtp.adminEmail,
        data: req.body,
        subject: `Nowa wiadomość od ${email} - formularz kontaktowy`,
        formData: req.body,
        templatePath: `${root}/emails/contact-form.ejs`,
        transporter,
      });
      res.json({ success: true });
    } catch (e) {
      res.send(e);
    }
  } else {
    res.json({ success: false, errors });
  }
};
