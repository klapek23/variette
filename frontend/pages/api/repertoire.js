import WPAPI from 'wpapi';
import moment from 'moment';
import Config from '../../config';

const wp = new WPAPI({ endpoint: Config.pl.apiUrl });
wp.repertoire = wp.registerRoute('wp/v2', 'repertoire/(?P<id>)');

export default async (req, res) => {
  let results;
  const { query } = req;

  try {
    results = await wp.repertoire().perPage(100).order('desc').orderby('date').embed();

    results = results.reduce((prev, current) => {
      const upcoming = current.acf.info && current.acf.info.upcoming && current.acf.info.upcoming.items
        ? current.acf.info.upcoming.items.map(({ date, link }) => ({
            direction: current.acf.direction,
            title: current.title,
            intro: current.acf.intro,
            info: current.acf.info,
            photos: current.acf.photos,
            details: current.acf.details,
            categories: current.categories,
            show_info_section: current.acf.show_info_section,
            show_photos_section: current.acf.show_photos_section,
            show_details_section: current.acf.show_details_section,
            date,
            slug: current.slug,
            buy: link,
          }))
        : [];

      return [...prev, ...upcoming];
    }, []);

    if (query.startDate) {
      results = results.filter(({ date }) => {
        return moment(date).isAfter(moment(query.startDate).subtract(1, 'day'), 'day');
      });
    }
    if (query.endDate) {
      results = results.filter(({ date }) => {
        return moment(date).isBefore(moment(query.endDate).add(1, 'day'), 'day');
      });
    }

    const promises = [];
    for (const item of results) {
      let cat;
      if (item.categories[0]) {
        if (item.categories[0] !== 9) {
          // archive
          cat = item.categories[0];
        } else {
          cat = item.categories[1];
        }
      }

      if (cat) {
        promises.push(wp.categories().id(cat));
      }
    }
    const promisesResults = await Promise.all(promises);

    results = results.map((item, i) => ({
      ...item,
      category: promisesResults[i],
    })).sort((a,b) => {
      const aDate = moment(a.date);
      const bDate = moment(b.date);
      return aDate > bDate ? 1 : -1;
    });

  } catch (error) {
    console.log(error);
  }
  return res.json(results);
};
