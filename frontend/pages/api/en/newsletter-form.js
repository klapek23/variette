import axios from 'axios';
import { isEmail } from '../../../utils/validators';
import messages from "../../../src/backend/messages";
import Config from "../../../config";

const config = Config.en;
const root = process.env.ROOT;

const validateForm = (form) => {
  const errors = {};
  const formFields = Object.keys(form);
  formFields.forEach((field) => {
    const value = form[field];
    switch (field) {
      case 'name':
        if (!value) {
          errors[field] = messages.en.required;
        }
        break;
      case 'rodo':
        if (!value) {
          errors[field] = messages.en.required;
        }
        break;
      case 'email':
        if (!value) {
          errors[field] = messages.en.required;
        }
        if (!isEmail(value)) {
          errors[field] = messages.en.email;
        }
        break;
      default:
        break;
    }
  });

  return Object.keys(errors).length > 0 ? errors : null;
};

export default async (req, res) => {
  const { email, name } = req.body;

  // Validate fields
  const errors = validateForm(req.body);

  if (!errors) {
    const request = {
      method: 'POST',
      url: 'https://api.freshmail.com/rest/subscriber/add',
      headers: {
        Authorization: config.freshmailToken,
        'Content-Type': 'application/json'
      },
      data: {
        email,
        list: config.freshmailListHash,
      }
    };
    const response = await axios(request);

    if (response.data.status === 'OK') {
      res.json({ success: true });
    } else {
      res.json({ success: false, errors: { freshmailerror: 'Freshmail api error' } });
    }
  } else {
    res.json({ success: false, errors });
  }
};
