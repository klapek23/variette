import React, {Component} from 'react';

import Layout from '../components/Layout';
import PageWrapper from '../components/PageWrapper';
import VrContactInfo from "../components/common/VrContactInfo";
import VrEqualBoxes from "../components/common/VrEqualBoxes";
import VrContactForm from "../components/common/forms/VrContactForm";
import {isEmail, isPhone} from "../utils/validators";
import VrBreadcrumbs from "../components/common/VrBreadcrumbs";
import VrGoogleMap from "../components/common/VrGoogleMap";

class Index extends Component {
  state = {
    displayGmaps: false
  }

  static async getInitialProps(ctx) {
    const {wp} = ctx;
    try {
      const [page] = await Promise.all([
        wp
          .pages()
          .slug('kontakt')
          .embed()
          .then((data) => {
            return data[0];
          }),
      ]);

      return {page};
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  render() {
    const {
      headerMenu,
      page,
      pathname,
      themeOptions,
      breadcrumbs,
    } = this.props;
    const {socials, ...otherThemeOptions} = themeOptions;
    const {acf} = page;
    const { section_01, section_02 } = acf;
    const { section_04 } = acf;
    const { map } = section_04;
    const latLng = {lat: map.lat, lng: map.lng};

    const formFields = [
      {
        defaultValue: '',
        type: 'text',
        name: 'name',
        fieldProps: {
          required: true,
          label: 'Twoje imię i nazwisko'
        },
        validationFn(value) {
          return !value
            ? 'To pole jest wymagane'
            : null;
        }
      },
      {
        type: 'double',
        fields: [
          {
            defaultValue: '',
            type: 'text',
            subtype: 'email',
            name: 'email',
            fieldProps: {
              required: true,
              label: 'Twój e-mail'
            },
            validationFn(value) {
              if (!value) {
                return 'To pole jest wymagane';
              }
              if (!isEmail(value)) {
                return 'Podaj poprawny adres e-mail'
              }
              return null;
            }
          },
          {
            defaultValue: '',
            type: 'text',
            subtype: 'phone',
            name: 'phone',
            fieldProps: {
              required: true,
              label: 'Telefon'
            },
            validationFn(value) {
              if (!value) {
                return 'To pole jest wymagane';
              }
              if (!isPhone(value)) {
                return 'Podaj poprawny numer telefonu'
              }
              return null;
            }
          },
        ]
      },
      {
        defaultValue: '',
        type: 'text',
        subtype: 'multiline',
        name: 'message',
        fieldProps: {
          required: true,
          label: 'Wiadomość',
          rowsMax: 4
        },
        validationFn(value) {
          return !value
            ? 'To pole jest wymagane'
            : null;
        }
      },
      {
        defaultValue: false,
        type: 'checkbox',
        name: 'newsletter',
        fieldProps: {
          label: "* Wyrażam zgodę na przetwarzanie moich danych osobowych przez Krakowski Teatr VARIETE w celu\n" +
            "udzielenia odpowiedzi na pytanie zadane przez formularz kontaktowy za pośrednictwem kanałów\n" +
            "komunikacji wskazanych w formularzu."
        },
      }
    ];

    return (
      <>
        <Layout menu={headerMenu} socials={socials} footerThemeOptions={otherThemeOptions} pathname={pathname}>
          <VrContactInfo
            title={section_01.title}
            subtitle={<VrBreadcrumbs items={breadcrumbs} pageTitle={section_01.title}/>}
            titleInfo={section_01.title_info}
            address={section_01.address}
            ticketOffice={section_01.ticket_office}
          />

          <VrEqualBoxes
            title={section_02.title}
            subtitle={section_02.subtitle}
            items={section_02.items}
          />

          <VrContactForm
            title="Wyślij wiadomość"
            successMessage="Wiadomośc wysłana pomyślnie"
            submitLabel="Wyślij"
            fields={formFields}
          />

          <VrGoogleMap
            latLng={latLng}
            map={map}
          />
        </Layout>
      </>
    );
  }
}

export default PageWrapper('pl')(Index);
