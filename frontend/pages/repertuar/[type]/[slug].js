import React, { Component } from 'react';
import moment from 'moment';

import Layout from '../../../components/Layout';
import PageWrapper from '../../../components/PageWrapper';
import VrEventIntro from '../../../components/common/VrEventIntro';
import VrContent02 from '../../../components/common/VrContent02';
import VrShowInfo from '../../../components/common/VrShow/VrShowInfo';
import VrShowPhotos from '../../../components/common/VrShow/VrShowPhotos';
import VrShowDetails from '../../../components/common/VrShow/VrShowDetails';
import VrPads from '../../../components/common/VrPads';
import VrBreadcrumbs from '../../../components/common/VrBreadcrumbs';
import VrShowAbout
  from '../../../components/common/VrShow/components/VrShowAbout';
import VrShowActors
  from '../../../components/common/VrShow/components/VrShowActors';
import VrShowCast
  from '../../../components/common/VrShow/components/VrShowCast';
import VrShowCover
  from '../../../components/common/VrShow/components/VrShowCover';
import VrShowReviews
  from '../../../components/common/VrShow/components/VrShowReviews';
import VrShowVideo
  from '../../../components/common/VrShow/components/VrShowVideo';

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentCastItem: {},
      mappedCast: [],
    };
  }

  static async getInitialProps({ query, wp }) {
    const { slug } = query;
    try {
      const [page] = await Promise.all([
        wp
          .repertoire()
          .slug(slug)
          .embed()
          .then((data) => {
            return data[0];
          }),
      ]);
      return { page };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  componentDidMount() {
    const { page } = this.props;
    const mappedCast = page.acf.details && page.acf.details.shows
      ? page.acf.details.shows.map(({ date, item }) => ({
        value: date,
        label: moment(date).format('DD MMMM YYYY'),
        items: item,
      }))
      : [];
    this.setState({
      currentCastItem: mappedCast[0],
      mappedCast,
    });
  }

  setCurrentCastItem = (item) => {
    this.setState({
      currentCastItem: item,
    });
  };

  render() {
    const {
      headerMenu,
      page,
      pathname,
      themeOptions,
      breadcrumbs,
    } = this.props;

    const { socials, ...otherThemeOptions } = themeOptions;
    const { acf } = page;
    const { intro, info, photos, details } = acf;
    const { mappedCast, currentCastItem } = this.state;

    const padsItems = [
      {
        title: 'Repertuar',
        as: '/repertuar/sprawdz-repertuar',
        href: '/repertuar/sprawdz-repertuar',
        label: 'Zobacz więcej',
      },
    ];

    const tabs = details ? [
      ...(details.content
        ? [
          {
            label: 'O sztuce',
            details: <VrShowAbout content={details.content} />,
          },
        ]
        : []),
      ...(details.actors
        ? [
          {
            label: 'Aktorzy',
            details: <VrShowActors actors={details.actors} />,
          },
        ]
        : []),
      ...(details.creators
        ? [
          {
            label: 'Twórcy',
            details: <VrShowActors actors={details.creators} />,
          },
        ]
        : []),
      ...(details.shows
        ? [
          {
            label: 'Obsada',
            details: (
              <VrShowCast
                cast={mappedCast}
                currentCastDate={currentCastItem}
                onCastDateChange={this.setCurrentCastItem}
                labels={{ check: 'Sprawdź obsadę' }}
              />
            ),
          },
        ]
        : []),
      ...(details.cover
        ? [
          {
            label: 'Plakat',
            details: <VrShowCover image={details.cover} />,
          },
        ]
        : []),
      ...(details.reviews
        ? [
          {
            label: 'Recenzje',
            details: (
              <VrShowReviews
                reviews={details.reviews}
                labels={{
                  readMore: 'Czytaj więcej',
                  collapse: 'Zwiń',
                }}
              />
            ),
          },
        ]
        : []),
      ...(details.video_url
        ? [
          {
            label: 'Video',
            details: (
              <VrShowVideo
                url={details.video_url}
                poster={details.video_cover && details.video_cover.url}
              />
            ),
          },
        ]
        : []),
    ] : [];

    return (
      <>
        <Layout
          menu={headerMenu}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >
          <VrEventIntro
            title={page.title.rendered}
            subtitle={<VrBreadcrumbs items={breadcrumbs}
                                     pageTitle={page.title.rendered} />}
            image={intro.image}
          />

          <VrContent02
            title={intro.title}
            subtitle={intro.subtitle}
            bg={{ color: '#b51d22' }}
          >
            <article dangerouslySetInnerHTML={{ __html: intro.content }} />
          </VrContent02>

          {page.acf.show_info_section && <VrShowInfo
            creators={info.creators}
            tickets={info.tickets}
            upcoming={info.upcoming}
          />}

          {page.acf.show_photos_section && photos.images && (
            <VrShowPhotos
              title={photos.title}
              subtitle={photos.subtitle}
              images={photos.images}
              labels={{
                creators: 'Twórcy',
                upcomingShows: 'Nadchodzące spektakle',
              }}
            />
          )}

          {tabs && tabs.length > 0 && (
            <VrShowDetails
              title={details.title}
              subtitle={details.subtitle}
              tabs={tabs}
            />
          )}

          <VrPads items={padsItems} className="vr-show__pads" />
        </Layout>
      </>
    );
  }
}

export default PageWrapper('pl')(Index);
