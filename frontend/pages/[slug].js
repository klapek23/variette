import React, { Component } from 'react';

import Layout from '../components/Layout';
import PageWrapper from '../components/PageWrapper';
import VrSubpageBanner from '../components/common/VrSubpageBanner';
import VrContentDefault from '../components/common/VrContentDefault';
import VrBreadcrumbs from '../components/common/VrBreadcrumbs';

class Index extends Component {
  static async getInitialProps(ctx) {
    const {wp, query, asPath, res} = ctx;
    const { slug } = query;

    if (asPath === '/repertuar') {
      res.writeHead(301, { location: "/repertuar/sprawdz-repertuar" } );
      res.end();
    }

    if (asPath === '/teatr') {
      res.writeHead(301, { location: "/teatr/bilety" } );
      res.end();
    }

    if (asPath === '/theatre') {
      res.writeHead(301, { location: "/theatre/tickets" } );
      res.end();
    }

    if (asPath === '/repertoir') {
      res.writeHead(301, { location: "/repertoir/check-repertoir" } );
      res.end();
    }

    try {
      const [page] = await Promise.all([
        wp
          .pages()
          .slug(slug)
          .embed()
          .then((data) => {
            return data[0];
          }),
      ]);

      return { page };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  render() {
    const {
      headerMenu,
      page,
      pathname,
      themeOptions,
      breadcrumbs,
    } = this.props;
    const { socials, ...otherThemeOptions } = themeOptions;
    const { acf } = page;
    const { main_banner } = acf;

    return (
      <>
        <Layout
          menu={headerMenu}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >
          <VrSubpageBanner
            title={main_banner.title}
            subtitle={
              <VrBreadcrumbs
                items={breadcrumbs}
                pageTitle={main_banner.title}
              />
            }
            dark
          />
          <VrContentDefault content={page.content.rendered} />
        </Layout>
      </>
    );
  }
}

export default PageWrapper('pl')(Index);
