import React, { Component } from 'react';
import moment from 'moment';
import axios from 'axios';

import Layout from '../../../components/Layout';
import PageWrapper from '../../../components/PageWrapper';
import VrRepertoire from '../../../components/common/VrRepertoire';
import VrBreadcrumbs from '../../../components/common/VrBreadcrumbs';

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      activeMonth: '',
      repertoire: [],
    };
  }

  static async getInitialProps({ req, wp, config }) {
    const ssr = !!req;

    try {
      const startDate = moment().startOf('month').format('YYYY-MM-DD');
      const endDate = moment().endOf('month').format('YYYY-MM-DD');
      const [page, repertoire] = await Promise.all([
        wp
          .pages()
          .slug('see-repertoir')
          .embed()
          .then((data) => {
            return data[0];
          }),
        ssr
          ? await axios.get(
              `/api/en/repertoire?startDate=${startDate}&endDate=${endDate}`,
              {
                baseURL: config.frontendUrl,
              },
            )
          : await axios.get(
              `/api/en/repertoire?startDate=${startDate}&endDate=${endDate}`,
            ),
      ]);

      return { page, repertoire: repertoire.data };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  componentDidMount() {
    const { repertoire } = this.props;
    this.setState({
      activeMonth: moment().format('M'),
      repertoire,
      loading: false,
    });
  }

  handleMonthChange = async (month, startDate, endDate) => {
    this.setState({
      loading: true,
    });

    const response = await axios.get(
      `/api/en/repertoire?startDate=${startDate}&endDate=${endDate}`,
    );

    this.setState({
      activeMonth: month,
      repertoire: response.data,
      loading: false,
    });
  };

  render() {
    const { headerMenu, pathname, themeOptions, breadcrumbs } = this.props;
    const { socials, ...otherThemeOptions } = themeOptions;
    const { repertoire, activeMonth, loading } = this.state;

    return (
      <>
        <Layout
          menu={headerMenu}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >
          <VrRepertoire
            title="Repertoire"
            subtitle={
              <VrBreadcrumbs items={breadcrumbs} pageTitle="Check repertoir" />
            }
            items={repertoire}
            activeMonth={activeMonth}
            onMonthChange={this.handleMonthChange}
            loading={loading}
            labels={{
              more: 'Read more',
              ticket: 'Buy ticket',
              noResults: 'Repertoire will appear soon.'
            }}
          />
        </Layout>
      </>
    );
  }
}

export default PageWrapper('en')(Index);
