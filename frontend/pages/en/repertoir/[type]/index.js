import React, { Component } from 'react';

import Layout from '../../../../components/Layout';
import PageWrapper from '../../../../components/PageWrapper';
import VrContentBoxes from '../../../../components/common/VrContentBoxes';
import VrPads from '../../../../components/common/VrPads';
import mapRepertoireToContentBoxes from '../../../../utils/mapRepertoireToContentBoxes';
import VrBreadcrumbs from '../../../../components/common/VrBreadcrumbs';

class Index extends Component {
  static async getInitialProps({ query, wp }) {
    const { type } = query;
    try {
      const [page, repertoire, categories] = await Promise.all([
        wp
          .pages()
          .slug(type)
          .embed()
          .then((data) => {
            return data[0];
          }),
        wp.repertoire().perPage(100).order('desc').orderby('date').embed(),
        wp.categories().perPage(100).embed(),
      ]);

      const category = categories.find(({ slug }) => {
        return slug === type;
      });

      const filteredRepertoire = repertoire.filter(
        ({ categories: itemCategories }) =>
          itemCategories.includes(category.id),
      );
      // add featured image
      filteredRepertoire.forEach(({ _embedded }, i) => {
        filteredRepertoire[i].image = _embedded['wp:featuredmedia'] ? _embedded['wp:featuredmedia'][0] : {};
      });

      return { page, repertoire: filteredRepertoire, type };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  render() {
    const {
      headerMenu,
      page,
      pathname,
      repertoire,
      themeOptions,
      breadcrumbs,
      type
    } = this.props;
    const { socials, ...otherThemeOptions } = themeOptions;
    const itemsToDisplay = mapRepertoireToContentBoxes(
      repertoire,
      `/repertoir/${type}`,
    );

    const padsItems = page.acf.links ? page.acf.links.map(({ link, label }) => ({
      ...link,
      label,
      as: link.url,
      href: '/repertoir/[type]'
    })) : [];

    return (
      <>
        <Layout
          menu={headerMenu}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >
          <div className="vr-repertoire-page">
            <VrContentBoxes
              title={page.title.rendered}
              subtitle={<VrBreadcrumbs items={breadcrumbs} pageTitle={page.title.rendered} />}
              items={itemsToDisplay}
              main
              gradientBottom={itemsToDisplay.length === 0}
            />
          </div>

          <VrPads items={padsItems} />
        </Layout>
      </>
    );
  }
}

export default PageWrapper('en')(Index);
