import React, {Component} from 'react';
import moment from 'moment';

import Layout from '../../../components/Layout';
import PageWrapper from '../../../components/PageWrapper';
import VrSubpageBanner from "../../../components/common/VrSubpageBanner";
import wpPosts2ContentBoxes from "../../../utils/news";
import VrNews from "../../../components/common/VrNews";
import VrBreadcrumbs from "../../../components/common/VrBreadcrumbs";

const perPage = 9;

class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: props.news
    }
  }

  static async getInitialProps({ query, wp }) {
    const { category, year } = query;

    try {
      const [page, news] = await Promise.all([
        wp
          .pages()
          .slug('news')
          .embed()
          .then((data) => {
            return data[0];
          }),
        // eslint-disable-next-line no-nested-ternary
        year
          ? wp
            .posts()
            .after(moment(`01/01/${year}`).format('L'))
            .before(moment(`01/01/${year}`).add(1, 'year').format('L'))
            .perPage(perPage)
            .page(1)
            .embed()
          : category
            ? wp
              .posts()
              .categories(category)
              .perPage(perPage)
              .page(1)
              .embed()
            : wp
              .posts()
              .perPage(perPage)
              .page(1)
              .embed()
      ]);

      return { page, news, query, totalPages: news._paging.totalPages, wp };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  componentDidMount() {
    const { news, totalPages } = this.props;
    this.totalPages = totalPages;
    this.setState({
      items: wpPosts2ContentBoxes(news)
    })
  }

  loadMoreNews = async () => {
    const { query, wp } = this.props;
    const { year, category } = query;
    const { items } = this.state;
    const nextPage = items.length / perPage + 1;
    // eslint-disable-next-line no-nested-ternary
    const newItems = year
      ? await wp
        .posts()
        .after(moment(`01/01/${year}`).format('L'))
        .before(moment(`01/01/${year}`).add(1, 'year').format('L'))
        .perPage(perPage)
        .page(nextPage)
        .embed()
          : category
          ? await wp
            .posts()
            .categories(category)
            .perPage(perPage)
            .page(nextPage)
            .embed()
          : await wp
            .posts()
            .perPage(perPage)
            .page(nextPage)
            .embed();

    this.setState({
      items: [...items, ...wpPosts2ContentBoxes(newItems)]
    });
  }

  render() {
    const {
      headerMenu,
      page,
      pathname,
      themeOptions,
      breadcrumbs
    } = this.props;
    const {socials, ...otherThemeOptions } = themeOptions;
    const {acf} = page;
    const { items: posts } = this.state;
    const nextPage = posts.length / perPage + 1;

    return (
      <>
        <Layout menu={headerMenu} socials={socials} footerThemeOptions={otherThemeOptions} pathname={pathname}>
          <VrSubpageBanner
            title={acf.main_banner.title}
            subtitle={<VrBreadcrumbs items={breadcrumbs} pageTitle={acf.main_banner.title} />}
            image={acf.main_banner.image}
            dark
          />

          <VrNews
            items={posts}
            white
            onButtonClick={this.loadMoreNews}
            disabled={this.totalPages < nextPage}
            label="More news"
          />
        </Layout>
      </>
    );
  }
}

export default PageWrapper('en')(Index);
