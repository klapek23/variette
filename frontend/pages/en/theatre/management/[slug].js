import React, {Component} from 'react';

import Layout from '../../../../components/Layout';
import PageWrapper from '../../../../components/PageWrapper';
import VrSubpageBanner from "../../../../components/common/VrSubpageBanner";
import VrContentDefault from "../../../../components/common/VrContentDefault";
import VrRepeatableSection from "../../../../components/common/VrRepeatableSection";
import VrBreadcrumbs from "../../../../components/common/VrBreadcrumbs";

class Index extends Component {

  static async getInitialProps({ query, wp }) {
    const { slug } = query;
    try {
      const [page, post] = await Promise.all([
        wp
          .pages()
          .slug('management')
          .embed()
          .then((data) => {
            return data[0];
          }),
        wp
          .direction()
          .slug(slug)
          .embed()
          .then((data) => {
            return data[0];
          }),
      ]);
      return { page, post }
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  render() {
    const {
      headerMenu,
      post,
      pathname,
      themeOptions,
      breadcrumbs
    } = this.props;
    const {socials, ...otherThemeOptions } = themeOptions;
    const featuredImage = post._embedded['wp:featuredmedia'][0];

    return (
      <>
        <Layout menu={headerMenu} socials={socials} footerThemeOptions={otherThemeOptions} pathname={pathname}>
          <VrSubpageBanner
            title={post.acf.main_banner.title}
            subtitle={<VrBreadcrumbs items={breadcrumbs} pageTitle={post.acf.main_banner.title} />}
            image={post.acf.main_banner.image}
            dark
          />

          <VrRepeatableSection image={{ url: featuredImage.source_url, alt: featuredImage.alt_text || featuredImage.title }}>
            <article>
              <h3>BIOGRAM</h3>
              <p dangerouslySetInnerHTML={{ __html: post.acf.extended_info }} />
            </article>
          </VrRepeatableSection>

          <VrContentDefault content={post.content.rendered} margin />
        </Layout>
      </>
    );
  }
}

export default PageWrapper('en')(Index);
