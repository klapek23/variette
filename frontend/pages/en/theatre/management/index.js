import React, { Component } from 'react';
import shortid from 'shortid';

import Layout from '../../../../components/Layout';
import PageWrapper from '../../../../components/PageWrapper';
import VrSubpageBanner from '../../../../components/common/VrSubpageBanner';
import VrRepeatableSection from '../../../../components/common/VrRepeatableSection';
import VrLink from '../../../../components/common/VrLink';
import VrButton from '../../../../components/common/VrButton';
import VrBreadcrumbs from "../../../../components/common/VrBreadcrumbs";

const DirectionItem = ({
  title,
  subtitle,
  image,
  content,
  contactInfo,
  slug,
  index,
}) => {
  const inverted = index % 2 !== 0;
  return (
    <VrRepeatableSection
      key={shortid.generate()}
      title={title}
      image={{ url: image.source_url, alt: image.alt_text || image.title }}
      link={{ as: `/theatre/management/${slug}`, href: '/theatre/management/[slug]' }}
      equalColumns
      inverted={inverted}
    >
      <article>
        <h3>{subtitle}</h3>
        <p>
          {contactInfo.map(({ type, value }) =>
            type.toLowerCase() === 'mail' ? (
              <span key={shortid.generate()}>
                {type}:{' '}
                <strong>
                  <a href={`mailto:${value}`} title={value} className="vr-link">
                    {value}
                  </a>
                </strong>
                <br />
              </span>
            ) : (
              <span key={shortid.generate()}>
                {type}: <strong>{value}</strong>
                <br />
              </span>
            ),
          )}
        </p>
        <div dangerouslySetInnerHTML={{ __html: content }} />
        <VrLink as={`/theatre/management/${slug}`} href="/theatre/management/[slug]">
          <VrButton>More</VrButton>
        </VrLink>
      </article>
    </VrRepeatableSection>
  );
};

class Index extends Component {
  static async getInitialProps({ wp }) {
    try {
      const [page, direction] = await Promise.all([
        wp
          .pages()
          .slug('management')
          .embed()
          .then((data) => {
            return data[0];
          }),
        wp.direction().embed(),
      ]);

      return { page, direction };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  render() {
    const { headerMenu, page, pathname, direction, themeOptions, breadcrumbs } = this.props;
    const { socials, ...otherThemeOptions } = themeOptions;
    const { acf } = page;
    const { main_banner } = acf;

    return (
      <>
        <Layout
          menu={headerMenu}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >
          <VrSubpageBanner
            title={main_banner.title}
            subtitle={<VrBreadcrumbs items={breadcrumbs} pageTitle={main_banner.title} />}
            image={main_banner.image}
            dark
          />

          {direction.map(({ title, acf, excerpt, slug, _embedded }, i) => (
            <DirectionItem
              key={shortid.generate()}
              title={title.rendered}
              subtitle={acf.position}
              contactInfo={acf.contact_info}
              image={_embedded['wp:featuredmedia'][0]}
              content={excerpt.rendered}
              slug={slug}
              index={i}
            />
          ))}
        </Layout>
      </>
    );
  }
}

export default PageWrapper('en')(Index);
