import React, { Component } from 'react';

import Layout from '../../../components/Layout';
import PageWrapper from '../../../components/PageWrapper';
import VrSubpageBanner from '../../../components/common/VrSubpageBanner';
import VrContent04 from '../../../components/common/VrContent04';
import VrBreadcrumbs from "../../../components/common/VrBreadcrumbs";
import VrPlayer from "../../../components/common/VrPlayer";

class Index extends Component {
  static async getInitialProps({wp}) {
    try {
      const [page] = await Promise.all([
        wp
          .pages()
          .slug('cooperation')
          .embed()
          .then((data) => {
            return data[0];
          }),
      ]);

      return { page };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  render() {
    const {
      headerMenu,
      page,
      pathname,
      themeOptions,
      breadcrumbs,
    } = this.props;
    const { socials, ...otherThemeOptions } = themeOptions;
    const { acf } = page;

    return (
      <>
        <Layout
          menu={headerMenu}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >
          <VrSubpageBanner
            title={acf.main_banner.title}
            subtitle={<VrBreadcrumbs items={breadcrumbs} pageTitle={acf.main_banner.title} />}
            image={acf.main_banner.image}
            dark
          />

          <VrContent04
            title={acf.section_01.title}
            link={acf.section_01.link}
            leftColumn={
              <article
                dangerouslySetInnerHTML={{ __html: acf.section_01.content }}
              />
            }
            rightColumn={
              <img
                src={acf.section_01.image.url}
                alt={acf.section_01.image.title}
              />
            }
            equalColumns
          />

          <VrContent04
            title={acf.section_02.title}
            link={acf.section_02.link}
            leftColumn={
              <article
                dangerouslySetInnerHTML={{ __html: acf.section_02.left_column }}
              />
            }
            rightColumn={
              <article
                dangerouslySetInnerHTML={{
                  __html: acf.section_02.right_column,
                }}
              />
            }
            equalColumns
            bothText
            alternate
          />

          {acf.video && <VrPlayer url={acf.video.url} cover={acf.video.poster} />}
        </Layout>
      </>
    );
  }
}

export default PageWrapper('en')(Index);
