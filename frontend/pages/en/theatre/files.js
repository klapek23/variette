import React, { Component } from 'react';

import Layout from '../../../components/Layout';
import PageWrapper from '../../../components/PageWrapper';
import VrSubpageBanner from '../../../components/common/VrSubpageBanner';
import VrBreadcrumbs from '../../../components/common/VrBreadcrumbs';
import VrFiles from '../../../components/common/VrFiles';

class Index extends Component {
  static async getInitialProps({wp}) {
    try {
      const [page] = await Promise.all([
        wp
          .pages()
          .slug('files')
          .embed()
          .then((data) => {
            return data[0];
          }),
      ]);

      return { page };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  render() {
    const { headerMenu, page, pathname, themeOptions, breadcrumbs } = this.props;
    const { socials, ...otherThemeOptions } = themeOptions;
    const { acf } = page;

    return (
      <>
        <Layout
          menu={headerMenu}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >
          <VrSubpageBanner
            title={acf.main_banner.title}
            subtitle={<VrBreadcrumbs items={breadcrumbs} pageTitle={acf.main_banner.title} />}
            dark
          />

          <VrFiles items={acf.files} />
        </Layout>
      </>
    );
  }
}

export default PageWrapper('en')(Index);
