import React, { Component } from 'react';

import Layout from '../../../components/Layout';
import PageWrapper from '../../../components/PageWrapper';
import VrSubpageBanner from '../../../components/common/VrSubpageBanner';
import VrBreadcrumbs from '../../../components/common/VrBreadcrumbs';
import VrContent03 from '../../../components/common/VrContent03';
import VrContent02 from '../../../components/common/VrContent02';
import VrContent03Animated from '../../../components/common/VrContent03Animated';

import IdeaSvg from '../../../static/idea.svg';
import BuildingSvg from '../../../static/building.svg';
import VrIframe from "../../../components/common/VrIframe";

class Index extends Component {
  static async getInitialProps({wp}) {
    try {
      const [page] = await Promise.all([
        wp
          .pages()
          .slug('history')
          .embed()
          .then((data) => {
            return data[0];
          }),
      ]);

      return { page };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  render() {
    const {
      headerMenu,
      page,
      pathname,
      themeOptions,
      breadcrumbs,
    } = this.props;
    const { socials, ...otherThemeOptions } = themeOptions;
    const { acf } = page;
    const {
      main_banner,
      section_01,
      section_02,
      section_03,
      section_04,
      video,
    } = acf;

    return (
      <>
        <Layout
          menu={headerMenu}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >
          <VrSubpageBanner
            title={main_banner.title}
            subtitle={
              <VrBreadcrumbs
                items={breadcrumbs}
                pageTitle={main_banner.title}
              />
            }
            image={main_banner.image}
            dark
          />

          <VrContent03Animated
            title={section_01.title}
            subtitle={section_01.subtitle}
            image={{ svg: IdeaSvg, id: 'idea-svg' }}
            className="history-01"
          >
            <article dangerouslySetInnerHTML={{ __html: section_01 ? section_01.content : '' }} />
          </VrContent03Animated>

          <VrContent02
            title={section_02.title}
            subtitle={section_02.subtitle}
            bg={{ color: '#b51d22', ...section_02.background }}
          >
            <article dangerouslySetInnerHTML={{ __html: section_02 ? section_02.content : '' }} />
          </VrContent02>

          <VrIframe>
            <article
              dangerouslySetInnerHTML={{ __html: acf.iframe ? acf.iframe.content : '' }}
            />
          </VrIframe>

          <VrContent03Animated
            title={section_03.title}
            subtitle={section_03.subtitle}
            image={{ svg: BuildingSvg, id: 'building-svg' }}
            inverted
            className="history-02"
          >
            <article dangerouslySetInnerHTML={{ __html: section_03 ? section_03.content : '' }} />
          </VrContent03Animated>

          <VrContent03
            title={section_04.title}
            subtitle={section_04.subtitle}
            image={section_04.image}
            bgColor="#1f1f1f"
            equalColumns
            white
          >
            <article dangerouslySetInnerHTML={{ __html: section_04 ? section_04.content : '' }} />
          </VrContent03>
        </Layout>
      </>
    );
  }
}

export default PageWrapper('en')(Index);
