import React, { Component } from 'react';
import VrLink from "../../components/common/VrLink";
import shortid from 'shortid';

import Layout from '../../components/Layout';
import PageWrapper from '../../components/PageWrapper';
import VrSubpageBanner from '../../components/common/VrSubpageBanner';
import VrBreadcrumbs from '../../components/common/VrBreadcrumbs';

const createDataTree = dataset => {
    const hashTable = Object.create(null);
    dataset.forEach(aData => hashTable[aData.id] = {...aData, childNodes: []});
    const dataTree = [];
    dataset.forEach(aData => {
        if(aData.parent) hashTable[aData.parent].childNodes.push(hashTable[aData.id]);
        else dataTree.push(hashTable[aData.id])
    });
    return dataTree;
};

class Index extends Component {
    static async getInitialProps({ wp, req, res, config }) {
        const ssr = !!res;

        try {
            const pages = await wp.pages().perPage(100);
            const management = await wp.direction();
            // const posts = await wp.posts().perPage(100);
            const categories = await wp.categories().perPage(100);
            let repertoire = await wp.repertoire().perPage(100);

            repertoire = repertoire.map((item) => {
                return { ...item, categories: item.categories.map(catId => categories.find(({ id }) => catId === id)) }
            });

            return {
                page: {
                    title: {
                        rendered: 'Variete Theatre - Sitemap'
                    },
                    excerpt: {
                        rendered: 'sitemap'
                    }
                },
                pages: createDataTree(pages.reverse()),
                management,
                repertoire,
                categories,
            };
        } catch (err) {
            console.log(err);
        }

        return {};
    }

    recursiveRenderItems = (sitemap) => {
        return sitemap.map((item) => (
            <li key={shortid.generate()}>
                {item.title && item.title.rendered && <VrLink as={item.url} href={item.url} prefetch={false}><span dangerouslySetInnerHTML={{ __html: item.title.rendered }} /></VrLink>}
                {item.childNodes && item.childNodes.length > 0 && <ul className="submenu">
                    {this.recursiveRenderItems(item.childNodes)}
                </ul>}
            </li>
        ))
    }

    render() {
        const {
            headerMenu,
            pathname,
            themeOptions,
            breadcrumbs,
            pages,
            management,
            repertoire,
            page,
        } = this.props;
        const { socials, ...otherThemeOptions } = themeOptions;

        const sitemap = pages.map((item) => {
            if (item.slug === 'theatre') {
                return { ...item, childNodes: item.childNodes.map((subitem) => {
                    if (subitem.slug === 'management') {
                        subitem.childNodes = management.map((node) => ({ ...node, url: `/theatre/management/${node.slug}` }))
                    }
                    return subitem;
                }) }
            }

            if (item.slug === 'repertoire') {
                return { ...item, childNodes: item.childNodes.map((subitem) => {
                    if (subitem.slug === 'shows') {
                        subitem.childNodes = repertoire.filter(({ categories }) => {
                            return categories[0].slug === 'shows';
                        }).map((node) => ({ ...node, url: `/repertoire/shows/${node.slug}` }))
                    }

                    if (subitem.slug === 'concerts') {
                        subitem.childNodes = repertoire.filter(({ categories }) => {
                            return categories[0].slug === 'concerts';
                        }).map((node) => ({ ...node, url: `/repertoire/concerts/${node.slug}` }))
                    }

                    if (subitem.slug === 'events') {
                        subitem.childNodes = repertoire.filter(({ categories }) => {
                            return categories[0].slug === 'events';
                        }).map((node) => ({ ...node, url: `/repertoire/events/${node.slug}` }))
                    }

                    if (subitem.slug === 'guest-shows') {
                        subitem.childNodes = repertoire.filter(({ categories }) => {
                            return categories[0].slug === 'guest-shows';
                        }).map((node) => ({ ...node, url: `/repertoire/guest-shows/${node.slug}` }))
                    }

                    if (subitem.slug === 'archive') {
                        subitem.childNodes = repertoire.filter(({ categories }) => {
                            return categories[0].slug === 'archive';
                        }).map((node) => ({ ...node, url: `/repertoire/archive/${node.slug}` }))
                    }

                    return subitem;
                }) }
            }

            /*if (item.slug === 'news') {
                item.childNodes = posts.map((node) => ({ ...node, url: `/news/${node.slug}` }))
            }*/

            return item;
        });

        return (
            <>
                <Layout
                    menu={headerMenu}
                    socials={socials}
                    footerThemeOptions={otherThemeOptions}
                    pathname={pathname}
                >
                    <VrSubpageBanner
                        title="Sitemap"
                        subtitle={
                            <VrBreadcrumbs
                                items={breadcrumbs}
                                pageTitle={page.title.rendered}
                            />
                        }
                        dark
                    />

                    <div className="vr-section">
                        <ul className="sitemap">
                            {this.recursiveRenderItems(sitemap)}
                        </ul>
                    </div>
                </Layout>
            </>
        );
    }
}

export default PageWrapper('en')(Index);
