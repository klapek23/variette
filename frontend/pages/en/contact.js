import React, {Component} from 'react';

import Layout from '../../components/Layout';
import PageWrapper from '../../components/PageWrapper';
import VrContactInfo from "../../components/common/VrContactInfo";
import VrEqualBoxes from "../../components/common/VrEqualBoxes";
import VrContactForm from "../../components/common/forms/VrContactForm";
import {isEmail, isPhone} from "../../utils/validators";
import VrBreadcrumbs from "../../components/common/VrBreadcrumbs";
import VrGoogleMap from "../../components/common/VrGoogleMap";

class Index extends Component {
  state = {
    displayGmaps: false
  }

  static async getInitialProps(ctx) {
    const {wp} = ctx;
    try {
      const [page] = await Promise.all([
        wp
          .pages()
          .slug('contact')
          .embed()
          .then((data) => {
            return data[0];
          }),
      ]);

      return {page};
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  render() {
    const {
      headerMenu,
      page,
      pathname,
      themeOptions,
      breadcrumbs,
    } = this.props;
    const {socials, ...otherThemeOptions} = themeOptions;
    const {acf} = page;
    const { section_01, section_02 } = acf;
    const { section_04 } = acf;
    const { map } = section_04;
    const latLng = {lat: map.lat, lng: map.lng};

    const formFields = [
      {
        defaultValue: '',
        type: 'text',
        name: 'name',
        fieldProps: {
          required: true,
          label: 'Full name'
        },
        validationFn(value) {
          return !value
            ? 'This field is required'
            : null;
        }
      },
      {
        type: 'double',
        fields: [
          {
            defaultValue: '',
            type: 'text',
            subtype: 'email',
            name: 'email',
            fieldProps: {
              required: true,
              label: 'E-mail'
            },
            validationFn(value) {
              if (!value) {
                return 'This field is required';
              }
              if (!isEmail(value)) {
                return 'This is not valid e-mail address'
              }
              return null;
            }
          },
          {
            defaultValue: '',
            type: 'text',
            subtype: 'phone',
            name: 'phone',
            fieldProps: {
              required: true,
              label: 'Phone number'
            },
            validationFn(value) {
              if (!value) {
                return 'This field is required';
              }
              if (!isPhone(value)) {
                return 'This is not valid phone number'
              }
              return null;
            }
          },
        ]
      },
      {
        defaultValue: '',
        type: 'text',
        subtype: 'multiline',
        name: 'message',
        fieldProps: {
          required: true,
          label: 'Message',
          rowsMax: 4
        },
        validationFn(value) {
          return !value
            ? 'This field is required'
            : null;
        }
      },
      {
        defaultValue: false,
        type: 'checkbox',
        name: 'newsletter',
        fieldProps: {
          label: "* I consent to the processing of my personal data by the VARIETE Theatre in Krakow for the purpose of answering the question asked via the contact form using the communication channels indicated in the form."
        },
      }
    ];

    return (
      <>
        <Layout menu={headerMenu} socials={socials} footerThemeOptions={otherThemeOptions} pathname={pathname}>
          <VrContactInfo
            title={section_01.title}
            subtitle={<VrBreadcrumbs items={breadcrumbs} pageTitle={section_01.title}/>}
            titleInfo={section_01.title_info}
            address={section_01.address}
            ticketOffice={section_01.ticket_office}
          />

          <VrEqualBoxes
            title={section_02.title}
            subtitle={section_02.subtitle}
            items={section_02.items}
          />

          <VrContactForm
            title="Write to us"
            successMessage="Message sent"
            submitLabel="Send"
            url="/api/en/contact-form"
            fields={formFields}
          />

          <VrGoogleMap
            latLng={latLng}
            map={map}
          />
        </Layout>
      </>
    );
  }
}

export default PageWrapper('en')(Index);
