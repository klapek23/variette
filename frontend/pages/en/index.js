import React, { Component } from 'react';

import Layout from '../../components/Layout';
import PageWrapper from '../../components/PageWrapper';
import VrSlider from '../../components/common/VrSlider';
import VrShows from '../../components/common/VrShows';
import VrNewsletterSection from '../../components/common/VrNewsletterSection';
import VrPlayer from '../../components/common/VrPlayer';
import VrClients from '../../components/common/VrClients';
import wpPosts2ContentBoxes from '../../utils/news';
import { isEmail } from '../../utils/validators';
import VrNews from '../../components/common/VrNews';
import VrOpenTicket from '../../components/common/VrOpenTicket';
import VrFrontPage from "../../components/common/VrFrontPage";

class Index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sliderPlay: false,
      frontPageClosed: false,
    }
  }

  static async getInitialProps({ wp, req}) {
    const ssr = !!req;

    try {
      const [page, posts] = await Promise.all([
        wp
          .pages()
          .slug('home-page')
          .embed()
          .then((data) => {
            return data[0];
          }),
        wp.posts().embed(),
      ]);

      const isMobileView = (req
          ? req.headers['user-agent']
          : navigator.userAgent
      ).match(
        /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
      );

      return { page, posts, isMobileView: !!isMobileView, ssr };
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  handleFrontPageClosed = () => {
    const { setFooterDisplay } = this.props;
    this.setState({ frontPageClosed: true, sliderPlay: true });
    setFooterDisplay(true);
  };

  render() {
    const { sliderPlay, frontPageClosed } = this.state;
    const { headerMenu, page, themeOptions, posts, pathname, isMobileView, config, ssr } = this.props;
    const lastPosts = wpPosts2ContentBoxes(posts).filter((item, i) => i < 3);
    const { socials, ...otherThemeOptions } = themeOptions;
    const { acf } = page;
    const {
      main_slider: mainSlider,
      news_section: newsSection,
      shows,
      open_ticket: openTicket,
      newsletter_section: newsletterSection,
      videoplayer,
      clients,
    } = acf;

    const newsletterFormFields = [
      {
        defaultValue: '',
        type: 'text',
        name: 'name',
        fieldProps: {
          required: true,
          label: 'Full name',
        },
        validationFn(value) {
          return !value ? 'This field is required' : null;
        },
      },
      {
        defaultValue: '',
        type: 'text',
        subtype: 'email',
        name: 'email',
        fieldProps: {
          required: true,
          label: 'E-mail',
        },
        validationFn(value) {
          if (!value) {
            return 'This field is required';
          }
          if (!isEmail(value)) {
            return 'This is not valid email address';
          }
          return null;
        },
      },
      {
        defaultValue: false,
        type: 'checkbox',
        name: 'rodo',
        fieldProps: {
          required: true,
          label:
            "* I consent to the processing of my personal data by the VARIETE Theatre in Krakow for the purpose of answering the question asked via the contact form using the communication channels indicated in the form.",
        },
        validationFn(value) {
          if (!value) {
            return 'To pole jest wymagane';
          }
          return null;
        },
      },
    ];

    return (
      <>
        {!isMobileView && <VrFrontPage title={config.title} subtitle="IMAGINE YOURSELF..." buttonLabel="ENTER" onClose={this.handleFrontPageClosed} />}

        <h1 className="hidden-title">Teatr Variete - Website of Krakow VARIETE theatre.</h1>

        <Layout
          menu={headerMenu}
          newsletterFormFields={newsletterFormFields}
          socials={socials}
          footerThemeOptions={otherThemeOptions}
          pathname={pathname}
        >
          {isMobileView || frontPageClosed ? <div>
            <VrSlider slides={mainSlider} play={sliderPlay} isMobile={isMobileView}  />

            <VrNews
              title={newsSection.title}
              subtitle={newsSection.subtitle}
              items={lastPosts}
              label="Więcej aktualności"
              animated
            />

            <VrShows title={shows.title} subtitle={shows.subtitle} items={shows.items} />

            <VrOpenTicket
              title={openTicket.title}
              subtitle={openTicket.subtitle}
              content={openTicket.content}
              bg="/static/open_ticket_bg.jpg"
              link={openTicket.link}
            />

            <VrNewsletterSection
              title={newsletterSection.title}
              subtitle={newsletterSection.subtitle}
              fields={newsletterFormFields}
              submitLabel="Subscribe"
              successMessage="Message sent"
              formUrl="/api/en/newsletter-form"
            />

            {videoplayer && <VrPlayer url={videoplayer.video_url} />}

            {clients && <VrClients items={clients.items} />}
          </div> : null}
        </Layout>
      </>
    );
  }
}

export default PageWrapper('en')(Index);
